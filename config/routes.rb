Spree::Core::Engine.routes.draw do

  # Add your extension routes here

  namespace :admin do
    resources :stickers do
      member do
        post :clone
      end
    end

    resources :templates do
      member do
        post :clone
      end
    end
    delete 'templates/:template_id/template_frames/:id', to: "template_frames#destroy", as: :template_frame

    resources :filters do
      member do
        post :clone
      end
    end

    resources :backgrounds do
      member do
        post :clone
      end
    end

    resources :products do 
      member do 
        post :get_taxons
      end
    end

    delete '/colors/:id', to: "colors#destroy", as: :color
  end
  resources :products do
    resources :personalize_products, only: [:new, :create, :edit, :update]
    get :get_product_variant
    get :save_gallery_photo
    get '/images/:id/download', to: "products#download", as: :download_image
  end
  resources :photo_galleries, only: [:show]
end
