Deface::Override.new(
  virtual_path: 'spree/shared/_gallery_product',
  name: 'modal_to_save_gallery_photo',
  insert_top: ".photo-gallery-blink",
  partial: "spree/products/select_gallery_photo"
)
