Deface::Override.new(
    :virtual_path   => "spree/admin/products/_form",
    :name           => "template_to_admin_product",
    :insert_bottom  => "[data-hook='admin_product_form_additional_fields']",
    :partial        =>  "spree/admin/products/add_template_to_products"
)