Deface::Override.new(
  virtual_path: 'spree/products/show',
  name: 'add_personalize_to_product_show',
  insert_top: "[data-hook='product_right_part_wrap']",
  partial:  "spree/products/add_personalize_button_to_product"
)
