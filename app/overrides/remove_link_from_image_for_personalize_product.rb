Deface::Override.new(
  virtual_path: 'spree/orders/_line_item',
  name: 'remove_link_to_for_personalize_product',
  surround: ".cart-item-image .col-3",
  partial:  "spree/orders/remove_link_of_personalize_product"
)