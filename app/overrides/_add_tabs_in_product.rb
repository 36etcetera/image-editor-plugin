Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'add_personalize_to_product_show',
  insert_bottom: "#new_product_wrapper",
  partial:  "spree/admin/products/add_tab_in_product"
)