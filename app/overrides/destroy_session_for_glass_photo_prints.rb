Deface::Override.new(
  virtual_path: 'spree/taxons/show',
  name: 'destroy_session_values',
  insert_after: ".taxons",
  partial: "spree/products/destroy_session_values"
)
