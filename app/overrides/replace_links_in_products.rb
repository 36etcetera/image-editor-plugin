Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'replace_links_in_index_page',
  replace: "[data-hook='admin_products_index_row_actions']",
  partial:  "spree/admin/products/links"
)