Deface::Override.new(
 virtual_path: "spree/admin/images/index",
 insert_bottom: "tbody",
 name: "add-product-associated-images",
 partial: "spree/admin/images/add_associated_pictures"
)