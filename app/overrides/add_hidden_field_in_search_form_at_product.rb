Deface::Override.new(
  virtual_path: 'spree/admin/products/index',
  name: 'add_hidden_field_in_search_form_at_product',
  insert_top: "[data-hook='admin_products_index_search_buttons']",
  partial:  "spree/admin/products/hidden_field_personalize"
)
