Deface::Override.new(:virtual_path => "spree/products/show", 
                     :name => "hide_stock_details_and_cart_btn", 
                     :surround => ".product-details",
                     :text => "<% unless @product.template && !@product.is_personalize_product?  %><%= render_original %><% end %>")