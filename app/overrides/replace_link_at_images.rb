Deface::Override.new(
  virtual_path: 'spree/admin/images/_image_row',
  name: 'replace_link_at_images',
  replace: ".actions",
  partial:  "spree/admin/images/links_for_images"
)
