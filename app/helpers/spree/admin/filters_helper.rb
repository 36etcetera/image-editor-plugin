module Spree::Admin::FiltersHelper
  def build_color_array(filter)
    color_arr = []
    filter.colors.each do |color|
      color_arr.push(color.try(:color_code))
    end

    color_arr.reverse
  end
end
