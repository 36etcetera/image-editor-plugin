module Spree::Admin::TemplatesHelper

  def number_of_images?(template)
    template.number_of_images.present? && template.number_of_images > 0
  end

  def number_of_text_boxes?(template)
    template.number_of_text_boxes.present? && template.number_of_text_boxes > 0
  end
  
  def textboxes_range(template)
    (1..template.number_of_text_boxes) if template.number_of_text_boxes?
  end

  def images_range(template)
    (1..template.number_of_images) if template.number_of_images?
  end

  def link_to_delete_frame(resource, options = {})
    url = options[:url] || object_url(resource)
    name = options[:name] || t('spree.actions.delete')
    link_to_with_icon 'trash', name, url, options
  end

end
