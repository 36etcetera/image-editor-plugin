module Spree::PersonalizeProductsHelper

  def render_preview_template_text(count, template_text)
    dummy_text = template_text.text_content
    template_font_family = template_text.font_family.gsub(/[+]/, ' ')
    content_tag(:div, class: "layer-doc layer-text", data: { 'layer-doc' => "layer-text-#{count}"}, style: "width: #{template_text.width}px; height: #{template_text.height}px; left: #{template_text.position_x}px; top: #{template_text.position_y}px; transform: rotate(#{template_text.rotate}deg); font-size: #{template_text.font_size}px!important; font-family: #{template_font_family} !important; color: #{template_text.font_color} !important;") do

      concat(content_tag(:span, "#{t('spree.personalize_product.text')}""-#{count}", class: 'doc-layer-name'))

      concat( content_tag(:div, dummy_text, class: "text-content", spellcheck: "false", contenteditable: "true" ))

      concat(content_tag(:span, class: 'editText') do
        concat(content_tag(:i, nil, :class => 'fa fa-edit'))
      end)
    end
  end

  def flexibility_for_background?(template)
    template.template_fields_permission.flexibility_for_background? if template_fields_permission?(template)
  end

  def flexibility_for_text?(template)
    template.template_fields_permission.flexibility_for_text? if template_fields_permission?(template)
  end

  def flexibility_for_sticker?(template)
    template.template_fields_permission.flexibility_for_sticker? if template_fields_permission?(template)
  end

  def background_color_or_img?(template)
    template.background_color.present? || template.background_image.present?
  end

  def template_fields_permission?(template)
    template.template_fields_permission.present?
  end

  def style_background(template)
    "width: #{template.width}px; height: #{template.height}px; border-radius: #{template.edges}px; background-size: 100% 100%;"
  end

  def parent_image_css
    "position: relative; overflow-x: hidden;overflow-y: hidden"
  end

  def style_template_images(template_img)
    "width: #{template_img.img_width}px; height: #{template_img.img_height}px; border-radius: #{template_img.border_radius}px; left: #{template_img.position_x}px; top: #{template_img.position_y}px; transform: rotate(#{template_img.rotate}deg); background: #fff; overflow: hidden;"
  end

  def get_template_img_title(template_img)
    "#{template_img.img_width}"+"x"+"#{template_img.img_height}"
  end

  def style_template_bg(template)
    style = style_background(template)
    bg_color = if template.background_color.present?
       "#{style} background: #{template.background_color};"
    end
    return style if bg_color.nil?
    return bg_color if bg_color.present?
  end

  def get_template_images(product)
    product.template.template_images
  end

  def template_frames?(product)
    product.template.template_frames.present?
  end

  def display_taxon_image(child)
    child.display_image.attachment(:product)
  end

  def display_product_images(product_images)
    if product_images.present?
      concat(content_tag(:div, 
        product_images.each do |sample_image|
          image_tag sample_image.attachment.url(:original)
        end,
      class: "owl-carousel-2 owl-carousel" ))
    else
      display_no_content_msg
    end
  end

  def display_no_content_msg
    content_tag(:div, t('spree.error.no_content'), class: "not-found-error")
  end
end
