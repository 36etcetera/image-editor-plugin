module Spree
  class PersonalizeProductsController < Spree::StoreController
    before_action :authenticate_spree_user!
    skip_before_action :verify_authenticity_token
    layout 'image_plugin'

    before_action :find_resource, only: [:new, :create]

    def new
      @template = @product.template
      return if @template.nil?
      @stickers = Spree::Sticker.all
      @background_patterns = Spree::Background.all
      @sample_images = @product.images
      @selected_variant_id = params[:variant_id]
      @select_gallery_photo = get_selected_gallery_photo
      @selected_photo_variant = session[:variant_id]

      @personalize_product.build_product_pictures(@product)
      @personalize_product.build_product_texts(@product)
      @personalize_product.product_stickers.build if @product.sticker_flexibility?
      @personalize_product.gallery_uploaded_images.build if @product.wishlist_photos?
      @personalize_product.build_product_background
    end

    def create
      begin
        @variant_id = product_associated_params[:product_variant]
        @new_product = @personalize_product.duplicate_for(@product, @variant_id)
        @new_product = CreatePersonalizeProduct.new(@new_product, product_associated_params, spree_current_user).call
        if @new_product.save
          respond_to do |format|
            format.js {}
            format.html
          end
        else
          flash[:error] = @new_product.errors.full_messages.join(", ")
        end
      rescue StandardError => e
        puts "======================Error is: #{e}====================="
      end
    end
    
  private
    def product_associated_params
      params.require(:personalize_product).permit(:image_data, :product_variant, product_background_attributes: {}, gallery_uploaded_images_attributes: {}, product_pictures_attributes: {}, product_stickers_attributes: {}, product_texts_attributes: {})
    end

    def get_selected_gallery_photo
      Spree::Product.find_by(slug: session[:picture_id])
    end

    def find_resource
      @product = Spree::Product.friendly.find params["product_id"]
      @personalize_product = Spree::PersonalizeProduct.new()
      raise ActionController::RoutingError.new('Not Found') if @product.template.nil? 
    end
  end
end
