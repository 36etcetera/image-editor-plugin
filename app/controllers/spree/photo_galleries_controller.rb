module Spree
  class PhotoGalleriesController < Spree::StoreController

    def show
      @taxon = Spree::Taxon.find_by!(permalink: params[:id])
      return unless @taxon

      # Use for Filter in Photo Gallery page
      if params[:selected_taxon].present? && !params[:selected_taxon].blank?
        @taxon_products = []
        params[:selected_taxon].each do |selected_taxon|
          taxon_childrens = @taxon.children.find_by(name: selected_taxon)
          @taxon_products.push(taxon_childrens.products)
        end
        @taxon_products = Kaminari.paginate_array(@taxon_products.flatten).page(params[:page]).per(params[:per_page])
      else
        @taxon_products = @taxon.products
      end
    end
  end
end
