module Spree
  module Admin
    class TemplatesController < ResourceController

      helper_method :clone_object_url

      def index
        session[:return_to] = request.url
        respond_with(@collection)
      end

      def new
        @template = Spree::Template.new
        @template.build_template_fields_permission
        @template.template_frames.build
      end

      def create
        @template = Spree::Template.new(permitted_resource_params)
        invoke_callbacks(:create, :before)
        if @template.save
          invoke_callbacks(:create, :after)
          flash[:success] = Spree.t('template.successfully_created')
          redirect_to edit_admin_template_path(@template)
        else
          invoke_callbacks(:create, :fails)
          respond_with(@template)
        end
      end

      def show
        redirect_to action: :edit
      end

      def edit
        @template.build_template_fields_permission if @template.template_fields_permission.blank?
        @template.build_template_images
        @template.build_template_texts
        @template.template_frames.build if @template.template_frames.blank?
      end

      def update
        @object.remove_template_images(permitted_resource_params[:number_of_images])
        @object.remove_template_texts(permitted_resource_params[:number_of_text_boxes])
        if @object.update_attributes(permitted_resource_params)
          flash[:success] = flash_message_for(@object, :successfully_updated)
          respond_with(@object) do |format|
            format.html { redirect_to spree.edit_admin_template_path(@template) }
            format.js   { render layout: false }
          end
        else
          # Stops people submitting blank slugs, causing errors when they try to
          # update the template again
          # @template.slug = @template.slug_was if @template.slug.blank?
          respond_with(@object)
        end
      end

      def destroy
        @template = Spree::Template.find(params[:id])
        if @template.products.present?
          @template.products.update(template_id: nil)
        end
        @template.discard
        flash[:success] = t('spree.notice_messages.template_deleted')
        respond_with(@template) do |format|
          format.html { redirect_to collection_url }
          format.js { render_js_for_destroy }
        end
      end

      def clone
        @new = @template.duplicate

        if @new.save
          flash[:success] = t('spree.notice_messages.template_cloned')
          redirect_to edit_admin_template_url(@new)
        else
          flash[:error] = t('spree.notice_messages.template_not_cloned', error: @new.errors.full_messages)
          redirect_to admin_templates_url
        end
      end

      private

        def find_resource
          Spree::Template.with_deleted.find(params[:id])
        end

        def collection
          return @collection if @collection
          params[:q] ||= {}
          params[:q][:s] ||= "name asc"
          # @search needs to be defined as this is passed to search_form_for
          @search = super.ransack(params[:q])
          @collection = @search.result.
                order(id: :asc).
                page(params[:page]).per(params[:per_page])
        end

        def clone_object_url(resource)
          clone_admin_template_url resource
        end
    end
  end
end
