module Spree
  module Admin
    ProductsController.class_eval do
        def get_taxons
          @product_taxon_ids = params[:product_taxon_ids].split(",")
          return if @product_taxon_ids.blank?

          taxons = Spree::Taxon.where(id: @product_taxon_ids)
          taxons_permalinks = taxons.map(&:permalink)
          glass_prints = ['glass-photo-prints', 'glass-photo-prints/new-node']
          @selected_taxon = glass_prints.any? { |x| taxons_permalinks.include?(x) }
        end

      private
        def collection
          return @collection if @collection
          params[:q] ||= {}
          params[:q][:s] ||= "name asc"
          # @search needs to be defined as this is passed to search_form_for
          @is_personalize_product = params[:personalize] == "true"
          @search = super.where(is_personalize_product: @is_personalize_product).ransack(params[:q])
          @collection = @search.result.
                order(id: :asc).
                page(params[:page]).per(params[:per_page])
        end
    end
  end
end