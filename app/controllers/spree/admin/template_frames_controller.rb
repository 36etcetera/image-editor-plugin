module Spree
  module Admin
    class TemplateFramesController < ResourceController

      def destroy
        @template_frame.destroy
      end

    private
      def find_resource
        template = Spree::Template.find(params[:template_id])
        template.template_frames.with_deleted.find params[:id]
      end
    end
  end
end
