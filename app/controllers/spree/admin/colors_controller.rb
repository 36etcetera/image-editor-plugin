module Spree
  module Admin
    class ColorsController < ResourceController
      def destroy
        color = Spree::Color.find(params[:id])
        color.destroy
        render plain: nil
      end
    end
  end
end
