module Spree
  module Admin
    class BackgroundsController < ResourceController

      helper_method :clone_object_url
      
      def index
        session[:return_to] = request.url
        respond_with(@collection)
      end

      def show
        redirect_to action: :edit
      end

      def update
        if @object.update_attributes(permitted_resource_params)
          flash[:success] = flash_message_for(@object, :successfully_updated)
          respond_with(@object) do |format|
            format.html { redirect_to spree.edit_admin_background_url }
            format.js   { render layout: false }
          end
        else
          @background.slug = @background.slug_was if @background.slug.blank?
          respond_with(@object)
        end
      end

      def destroy
        @background = Spree::Background.find(params[:id])
        @background.discard

        flash[:success] = t('spree.notice_messages.background_deleted')

        respond_with(@background) do |format|
          format.html { redirect_to collection_url }
          format.js { render_js_for_destroy }
        end
      end

      def clone
        @new = @background.duplicate

        if @new.save
          flash[:success] = t('spree.notice_messages.background_cloned')
          redirect_to edit_admin_background_url(@new)
        else
          flash[:error] = t('spree.notice_messages.background_not_cloned', error: @new.errors.full_messages)
          redirect_to admin_backgrounds_url
        end
      end

      private
        def find_resource
          Spree::Background.with_deleted.find(params[:id])
        end

        def collection
          return @collection if @collection
          params[:q] ||= {}
          params[:q][:s] ||= "name asc"
          # @search needs to be defined as this is passed to search_form_for
          @search = super.ransack(params[:q])
          @collection = @search.result.
                order(id: :asc).
                page(params[:page]).per(params[:per_page])
        end

        def clone_object_url(resource)
          clone_admin_background_url resource
        end
    end
  end
end
