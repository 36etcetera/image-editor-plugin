module Spree
  OrdersController.class_eval do
    def populate
      @order   = current_order(create_order_if_necessary: true)
      variant  = Spree::Variant.find(params[:variant_id])
      quantity = params[:quantity].present? ? params[:quantity].to_i : 1

      gallery_images = variant.product.gallery_uploaded_images
      if gallery_images.present?
        gallery_images.each do |gallery_img|
          variant_id = gallery_img.variant_id
          quantity = gallery_img.quantity
          product_variant  = Spree::Variant.find(variant_id)
          @order_item = @order.contents.add(product_variant, quantity)
        end
      end

      # 2,147,483,647 is crazy. See issue https://github.com/spree/spree/issues/2695.
      if !quantity.between?(1, 2_147_483_647)
        @order.errors.add(:base, t('spree.please_enter_reasonable_quantity'))
      end

      begin
        @line_item = @order.contents.add(variant, quantity)
      rescue ActiveRecord::RecordInvalid => e
        @order.errors.add(:base, e.record.errors.full_messages.join(", "))
      end

      respond_with(@order) do |format|
        format.html do
          if @order.errors.any?
            flash[:error] = @order.errors.full_messages.join(", ")
            redirect_back_or_default(spree.root_path)
            return
          else
            redirect_to cart_path
          end
        end
      end
    end
  end
end
