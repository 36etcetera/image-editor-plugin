module Spree
  ProductsController.class_eval do
    before_action :get_product, only: [:save_gallery_photo, :download]

    def save_gallery_photo
      session[:picture_id] = params[:product_id]
      if params[:variant].present?
        session[:variant_id] = params[:variant]
      else
        session[:variant_id] = @product.master.id
      end
      redirect_to nested_taxons_path('glass-photo-prints')
    end

    def download
      case params[:tablename]
        when "product_background"
          image = eval_str(@product, params[:tablename])
        when "product_stickers"
          image = Spree::Sticker.find params[:id]
        else
          image = eval_str(@product, params[:tablename]).find params[:id]
      end
      send_file(eval_str(image, params[:fieldname]).path(:original), filename: eval("image.#{params[:fieldname]}_file_name"))
    end
    
  private
    def eval_str(image, val)
      eval("image.#{val}")
    end

    def get_product
      @product = Spree::Product.find_by(slug: params[:product_id])
    end
  end
end
