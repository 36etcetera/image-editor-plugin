module Spree
  class TemplateDuplicator
    attr_accessor :template

    def initialize(template)
      @template = template
    end

    def duplicate
      new_template = duplicate_template

      new_template
    end

    protected

    def duplicate_template
      temp_bg_image = template.background_image
      temp_bg_color = template.background_color
      template.dup.tap do |new_template|
        new_template.name = "COPY OF #{template.name}"
        new_template.created_at = nil
        new_template.deleted_at = nil
        new_template.updated_at = nil
        new_template.description = template.description
        new_template.width = template.width
        new_template.height = template.height
        new_template.edges = template.edges

        new_template.background_image = temp_bg_image if temp_bg_image.present?
        new_template.background_color = temp_bg_color if temp_bg_color.present?

        new_template.number_of_images = template.number_of_images
        new_template.number_of_text_boxes = template.number_of_text_boxes

        new_template.template_images = template.template_images.map { |image| duplicate_images image } if template.number_of_images.present?

        new_template.template_texts = template.template_texts.map { |text| duplicate_texts text } if template.number_of_text_boxes.present?

        new_template.template_fields_permission = duplicate_fields_property(@template)

      end
    end

    def duplicate_images(image)
      new_image = image.dup
      new_image.created_at = nil
      new_image.updated_at = nil
      new_image
    end

    def duplicate_texts(text)
      new_text = text.dup
      new_text.created_at = nil
      new_text.updated_at = nil 
      new_text
    end

    def duplicate_fields_property(template)
      new_prop = template.template_fields_permission.dup
      new_prop.created_at = nil
      new_prop.updated_at = nil
      new_prop
    end
  end
end
