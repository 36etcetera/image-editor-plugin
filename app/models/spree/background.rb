module Spree
  class Background < Spree::Base
    acts_as_paranoid
    include Spree::ParanoiaDeprecations

    include Discard::Model
    self.discard_column = :deleted_at

    validates_presence_of :name, :image

    has_attached_file :image,
               styles: { mini: '48x48>', small: '100x100>', product: '240x240>', large: '600x600>' },
                default_style: :background,
                url: '/spree/backgrounds/:id/:style/:basename.:extension',
                path: ':rails_root/public/spree/backgrounds/:id/:style/:basename.:extension',
                convert_options: { all: '-strip -auto-orient -colorspace sRGB' }
    validates_attachment :image,
      content_type: { content_type: %w(image/jpeg image/jpg image/png image/gif) }

    def duplicate
      duplicator = BackgroundDuplicator.new(self)
      duplicator.duplicate
    end
  end
end
