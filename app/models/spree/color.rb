require 'discard'

module Spree
  class Spree::Color < Spree::Base
    acts_as_paranoid
    include Spree::ParanoiaDeprecations

    include Discard::Model
    self.discard_column = :deleted_at
    
    validates_presence_of :color_code

    belongs_to :filter, inverse_of: :colors, class_name: 'Spree::Filter'
  end
end
