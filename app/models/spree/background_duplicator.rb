module Spree
  class BackgroundDuplicator
    attr_accessor :background

    def initialize(background)
      @background = background
    end

    def duplicate
      new_background = duplicate_background

      new_background
    end

    protected

    def duplicate_background
      background.dup.tap do |new_background|
        new_background.name = "COPY OF #{background.name}"
        new_background.created_at = nil
        new_background.deleted_at = nil
        new_background.updated_at = nil
        new_background.image = background.image
      end
    end
  end
end
