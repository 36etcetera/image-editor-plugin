Spree::Product.class_eval do

  belongs_to :template
  has_many :product_pictures, dependent: :destroy
  has_many :product_stickers, dependent: :destroy
  has_many :product_texts, dependent: :destroy
  has_many :gallery_uploaded_images
  has_one :product_background, dependent: :destroy

  accepts_nested_attributes_for :product_pictures, :product_texts, :product_background, :product_stickers, :gallery_uploaded_images
  after_create :set_stock_value

  def sticker_flexibility?
    fields_permission = template.template_fields_permission
    return if fields_permission.nil?
    fields_permission.flexibility_for_sticker?
  end

  def set_stock_value
    if is_personalize_product?
      master.stock_items.each do |stock_item|
        stock_item.update_column(:count_on_hand, 999)
        stock_item.update(backorderable: false)
      end
    end
  end
end
