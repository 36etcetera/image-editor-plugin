class Spree::GalleryUploadedImage < Spree::Base
  belongs_to :product

  before_save :update_product_details

private

  def update_product_details
    if self.variant_id.nil?
      product = Spree::Product.find(self.uploaded_photo_id)
      self.variant_id = product.master.id
     end
  end
end
