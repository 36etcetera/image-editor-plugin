module Spree
  class StickerDuplicator
    attr_accessor :sticker

    def initialize(sticker)
      @sticker = sticker
    end

    def duplicate
      new_sticker = duplicate_sticker

      new_sticker
    end

    protected

    def duplicate_sticker
      sticker.dup.tap do |new_sticker|
        new_sticker.name = "COPY OF #{sticker.name}"
        new_sticker.created_at = nil
        new_sticker.deleted_at = nil
        new_sticker.updated_at = nil
        new_sticker.image = sticker.image
      end
    end
  end
end
