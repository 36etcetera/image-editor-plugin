require 'discard'

module Spree
  class Template < Spree::Base
    attr_accessor :remove_background_image, :background

    acts_as_paranoid
    include Spree::ParanoiaDeprecations

    include Discard::Model
    self.discard_column = :deleted_at

    has_many :products
    has_many :template_images
    has_many :template_texts
    has_many :template_frames, dependent: :destroy, inverse_of: :template
    has_one :template_fields_permission

    accepts_nested_attributes_for :template_images, :template_texts, :template_fields_permission, allow_destroy: :true
    accepts_nested_attributes_for :template_frames, allow_destroy: true, reject_if: lambda {|att| att['frame_image'].blank? }

    validates_presence_of :name, :width, :height
    validates :width, :height, numericality: { greater_than: 0 }
    validate :image_or_color

    has_attached_file :background_image,
               styles: { mini: '48x48>', small: '100x100>', product: '240x240>', large: '600x600>' },
                default_style: :image,
                url: '/spree/template/background_images/:id/:style/:basename.:extension',
                path: ':rails_root/public/spree/template/background_images/:id/:style/:basename.:extension',
                convert_options: { all: '-strip -auto-orient -colorspace sRGB' }

    validates_attachment :background_image, content_type: { content_type: %w(image/jpeg image/jpg image/png image/gif) }

    before_save :delete_background_image

    def slug_candidates
      [
        :name
      ]
    end

    def deleted?
      !!deleted_at
    end

    # Creates a new template with the same attributes.
    #
    # @return [Spree::Template] the duplicate
    def duplicate
      duplicator = TemplateDuplicator.new(self)
      duplicator.duplicate
    end

    def build_template_images
      if(!template_images.present? && number_of_images.present? && number_of_images > 0)
        build_images(number_of_images)
      else
        build_images(number_of_images - template_images.count) if template_images.present?
      end
    end

    def build_template_texts
      if (!template_texts.present? && number_of_text_boxes.present? && number_of_text_boxes > 0)
        build_texts(number_of_text_boxes)
      else
        build_texts(number_of_text_boxes - template_texts.count) if template_texts.present?
      end
    end

    def remove_template_images(count_images)
      count_images = count_images.to_i
      if template_images.present? && template_images.count > count_images
        remaining_images = (template_images.count - count_images)
        template_images.order('id desc').limit(remaining_images).destroy_all
      end
    end

    def remove_template_texts(count_texts)
      count_texts = count_texts.to_i
      if template_texts.present? && template_texts.count > count_texts
        remaining_texts = (template_texts.count - count_texts)
        template_texts.order('id desc').limit(remaining_texts).destroy_all
      end
    end

  private

    def build_images(no_of_images)
      no_of_images.times  {
                            template_images.build
                          }
    end

    def build_texts(no_of_texts)
      no_of_texts.times  {
                            template_texts.build
                          }
    end

    def image_or_color
      if background_image.blank? && background_color.blank?
        errors.add(:base, I18n.t('spree.template.background_error'))
      end
    end

    def delete_background_image
      self.background_image.clear if remove_background_image == "1"
    end
  end
end
