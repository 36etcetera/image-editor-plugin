class Spree::ProductPicture < Spree::Base
  attr_accessor :rotate_image
  
  belongs_to :product
  has_attached_file :product_image,
               styles: { mini: '48x48>', small: '100x100>', product: '240x240>', large: '600x600>' },
                default_style: :image,
                url: '/spree/product_images/:id/:style/:basename.:extension',
                path: ':rails_root/public/spree/product_images/:id/:style/:basename.:extension',
                convert_options: { all: '-strip -auto-orient -colorspace sRGB' }
  validates_attachment :product_image, content_type: { content_type: %w(image/jpeg image/jpg image/png image/gif) }
end
