Spree::Image.class_eval do
  after_create :post_image_processing, if: Proc.new{ |obj| obj.viewable.product.is_personalize_product? }

private

  def post_image_processing
    ResizeProductImageJob.set(wait: 1.minute).perform_later(self.id)
   end
end
