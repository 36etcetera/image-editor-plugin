class Spree::TemplateText < Spree::Base
  belongs_to :template

  validates_presence_of :width, :height
  validates :width, :height, numericality: { greater_than: 0 }
end
