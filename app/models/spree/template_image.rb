class Spree::TemplateImage < Spree::Base
  belongs_to :template

  validates_presence_of :img_width, :img_height
  validates :img_width, :img_height, numericality: { greater_than: 0 }
end
