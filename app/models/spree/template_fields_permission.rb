module Spree
  class TemplateFieldsPermission < Spree::Base
    belongs_to :template
  end
end
