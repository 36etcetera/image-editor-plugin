module Spree
  class FilterDuplicator
    attr_accessor :filter

    def initialize(filter)
      @filter = filter
    end

    def duplicate
      new_filter = duplicate_filter

      new_filter.save!
      new_filter
    end

    protected

    def duplicate_filter
      filter.dup.tap do |new_filter|
        new_filter.name = "COPY OF #{filter.name}"
        new_filter.created_at = nil
        new_filter.deleted_at = nil
        new_filter.updated_at = nil
        new_filter.colors = filter.colors.map { |color| duplicate_color color }
      end
    end

    def duplicate_color(color)
      new_color = color.dup
      new_color.created_at = nil
      new_color.deleted_at = nil
      new_color.updated_at = nil

      new_color
    end
  end
end

