require 'discard'

class Spree::TemplateFrame < Spree::Base
  acts_as_paranoid
  include Spree::ParanoiaDeprecations

  include Discard::Model
    self.discard_column = :deleted_at

  belongs_to :template, inverse_of: :template_frames
  has_attached_file :frame_image,
               styles: { mini: '48x48>', small: '100x100>', product: '240x240>', large: '600x600>' },
                default_style: :image,
                url: '/spree/template/frame_images/:id/:style/:basename.:extension',
                path: ':rails_root/public/spree/template/frame_images/:id/:style/:basename.:extension',
                convert_options: { all: '-strip -auto-orient -colorspace sRGB' }
  validates_attachment :frame_image, content_type: { content_type: %w(image/jpeg image/jpg image/png image/gif) }
end
