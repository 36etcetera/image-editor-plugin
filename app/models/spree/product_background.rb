class Spree::ProductBackground < Spree::Base
  attr_accessor :product_background_data
  belongs_to :product
  has_attached_file :background_image,
               styles: { mini: '48x48>', small: '100x100>', product: '240x240>', large: '600x600>' },
                default_style: :background,
                url: '/spree/product_background/:id/:style/:basename.:extension',
                path: ':rails_root/public/spree/product_background/:id/:style/:basename.:extension',
                convert_options: { all: '-strip -auto-orient -colorspace sRGB' }
  validates_attachment :background_image, content_type: { content_type: %w(image/jpeg image/jpg image/png image/gif) }

  before_create :set_dimensions

private
  def set_dimensions
    self.background_width = product.template.width
    self.background_height = product.template.height
  end
end

