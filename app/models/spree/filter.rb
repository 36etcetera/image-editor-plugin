require 'discard'

module Spree
  class Filter < Spree::Base
    acts_as_paranoid
    include Spree::ParanoiaDeprecations

    include Discard::Model
    self.discard_column = :deleted_at

    validates_presence_of :name

    has_many :colors, inverse_of: :filter, class_name: 'Spree::Color', dependent: :destroy
    accepts_nested_attributes_for :colors, allow_destroy: true, reject_if: proc {|att| att['color_code'].blank?}

    after_discard do
      colors.discard_all
    end

    def deleted?
      !!deleted_at
    end

    def duplicate
      duplicator = FilterDuplicator.new(self)
      duplicator.duplicate
    end

    def slug_candidates
      [
        :name
      ]
    end
  end
end
