require 'discard'

module Spree
  class Sticker < Spree::Base
    acts_as_paranoid
    include Spree::ParanoiaDeprecations

    include Discard::Model
    self.discard_column = :deleted_at

    validate :no_attachment_errors

    validates_presence_of :name, :image

    has_attached_file :image,
               styles: { mini: '48x48>', small: '100x100>', product: '240x240>', large: '600x600>' },
                default_style: :sticker,
                url: '/spree/stickers/:id/:style/:basename.:extension',
                path: ':rails_root/public/spree/stickers/:id/:style/:basename.:extension',
                convert_options: { all: '-strip -auto-orient -colorspace sRGB' }
    validates_attachment :image,
      presence: true,
      content_type: { content_type: %w(image/png), message: 'only allow png' }

    validates_with AttachmentSizeValidator, attributes: :image, less_than: 3.megabytes

    validate :check_dimensions


    def check_dimensions
      temp_file = image.queued_for_write[:original]
      unless temp_file.nil?
        dimensions = Paperclip::Geometry.from_file(temp_file)
        width = dimensions.width
        height = dimensions.height
        if width < 2000 && height < 2000
          errors['image'] << I18n.t('spree.sticker.image.dimension_error')
        # elsif width != height
        #   errors['image'] << I18n.t('spree.sticker.image.box_error')
        end
      end
    end

    # if there are errors from the plugin, then add a more meaningful message
    def no_attachment_errors
      unless image.errors.empty?
        # uncomment this to get rid of the less-than-useful interim messages
        # errors.clear
        errors.add :image, "Paperclip returned errors for file '#{image_file_name}' - check ImageMagick installation or image source file."
        false
      end
    end

    # Try building a slug based on the following fields in increasing order of specificity.
    def slug_candidates
      [
        :name
      ]
    end

    def deleted?
      !!deleted_at
    end

    # Creates a new sticker with the same attributes.
    #
    # @return [Spree::Sticker] the duplicate
    def duplicate
      duplicator = StickerDuplicator.new(self)
      duplicator.duplicate
    end

  end
end
