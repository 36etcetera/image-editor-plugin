module Spree
  class PersonalizeProduct < Spree::Product
    attr_accessor :image_data, :product_variant

    belongs_to :personalize_by, class_name: 'Spree::Product'

    has_many :product_pictures, foreign_key: 'product_id', dependent: :destroy
    has_many :product_texts, foreign_key: 'product_id', dependent: :destroy
    has_many :product_stickers, foreign_key: 'product_id', dependent: :destroy
    has_many :gallery_uploaded_images, foreign_key: 'product_id', dependent: :destroy
    has_one :product_background, foreign_key: 'product_id', dependent: :destroy

    accepts_nested_attributes_for :product_pictures, :product_texts, :product_background, :product_stickers, :gallery_uploaded_images

    def duplicate_for(product, variant_id=nil)
      new_product = duplicate_product(product, variant_id)

      # don't dup the actual variants, just the characterising types
      new_product.option_types = product.option_types if product.has_variants?

      # allow site to do some customization
      new_product.send(:duplicate_extra, product) if new_product.respond_to?(:duplicate_extra)
      
      new_product
    end

    def duplicate_product(product, variant_id)
      append_number = Time.now.to_i      
      product.dup.tap do |new_product|
        I18n.available_locales.each do |locale|
          I18n.with_locale(locale) {
            new_product.name = "#{product.name}"
            new_product.slug = "#{product.slug}-#{append_number}"
          }
        end
        new_product.taxons = product.taxons
        new_product.created_at = DateTime.current
        new_product.deleted_at = nil
        new_product.updated_at = DateTime.current
        new_product.product_properties = reset_properties(product)
        new_product.master = duplicate_master(product, variant_id)
        new_product.is_personalize_product = true
        new_product.personalize_by_id = product.id
        new_product.height = product.height
        new_product.width = product.width
      end
    end

    def duplicate_master(product, variant_id)
      master = product.master
      master.dup.tap do |new_master|
        new_master.sku = "#{master.sku}-#{generate_code(4)}"
        new_master.deleted_at = nil
        new_master.price = get_product_price(variant_id, master)
        new_master.weight = get_product_weight(variant_id, master)
      end
    end

    def get_product_price(variant_id, master)
      selected_variant = get_variant(master, variant_id)
      selected_variant.present? ? selected_variant.price : master.price
    end

    def get_product_weight(variant_id, master)
      selected_variant = get_variant(master, variant_id)
      selected_variant.present? ? selected_variant.weight : master.weight
    end

    def reset_properties(product)
      product.product_properties.map do |prop|
        prop.dup.tap do |new_prop|
          new_prop.created_at = nil
          new_prop.updated_at = nil
        end
      end
    end

    def generate_code(number)
      charset = Array('A'..'Z') + Array('a'..'z')
      Array.new(number) { charset.sample }.join
    end

    def build_product_pictures(product)
      temp_images = product.template.template_images
      if temp_images.present?
        no_of_images = temp_images.count
        no_of_images.times { product_pictures.build }
      end
    end

    def build_product_texts(product)
      temp_texts = product.template.template_texts
      if temp_texts.present?
        no_of_texts = temp_texts.count
        no_of_texts.times { product_texts.build }
      else
        product_texts.build
      end
    end

    private
      def get_variant(master, variant_id)
        master.product.variants.find(variant_id) unless variant_id.blank?
      end
  end
end
