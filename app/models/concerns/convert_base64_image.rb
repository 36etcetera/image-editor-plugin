module ConvertBase64Image
  include ActiveSupport::Concern
    
  def generate_background_image(img, product_name)
    img_base64_to_file = img_data(img)
    extension = get_img_extension(img) 

    if img_base64_to_file
      img_base64_to_file.original_filename = "#{product_name} background image#{Time.now.to_i}.#{extension}"
      img_base64_to_file
    end
  end

  def img_data(image)
    decoded_data = Base64.decode64(image.split(',').last)

    data = StringIO.new(decoded_data)
    data.class_eval do
      attr_accessor :content_type, :original_filename
    end
    data.content_type = "image/png"
    return data
  end

  def generate_image(product)
    image_record = img_data(@params[:image_data])
    if image_record
      image_record.original_filename = "#{product.name}-#{Time.now.to_i}.png"
      Spree::Image.new(attachment: image_record)
    end
  end

  def get_img_extension(img)
    img_extension = img.split(',')[0]
    img_extension.match(/gif|jpeg|png|jpg/).to_s
  end
end