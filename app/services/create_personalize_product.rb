class CreatePersonalizeProduct
  include ConvertBase64Image

  def initialize(product, params, spree_current_user)
    @product = product
    @params = params
    @user = spree_current_user
  end

  def call
    @product.product_pictures.build(modify_product_image_params)
    @product.build_product_background(modify_product_background_params)
    @product.gallery_uploaded_images.build(modify_photo_gallery_params)
    @product.product_stickers.build(modify_product_stickers_params)
    @product.product_texts.build(modify_product_texts_params)
    upload_background_image(@product)
    upload_frame(@product)
    @product.master.images << generate_image(@product)
    @product
  end

  private
    def product_background_params
      @params[:product_background_attributes].presence || {}
    end

    def product_pictures_attributes
      @params[:product_pictures_attributes].presence || {}
    end

    def photo_gallery_attributes
      @params[:gallery_uploaded_images_attributes].presence || {}
    end

    def product_stickers_attributes
      @params[:product_stickers_attributes].presence || {}
    end

    def product_texts_attributes
      @params[:product_texts_attributes].presence || {}
    end

    def modify_product_image_params
      product_pictures_arr = []
      product_pictures_attributes.each do |key, value_hash|
        product_pictures_arr << value_hash if value_hash[:product_image].present?
      end
      product_pictures_arr
    end

    def modify_product_background_params
      product_bg_data = product_background_params[:product_background_data]
      return product_background_params unless product_bg_data.present?

      set_background_image_params(product_bg_data, product_background_params)
    end

    def modify_photo_gallery_params
      photo_gallery_uploaded_photo_arr = []

      photo_gallery_attributes.each do |key, value_hash|
        photo_gallery_uploaded_photo_arr << value_hash unless value_hash[:uploaded_photo_id].blank?
      end
      photo_gallery_uploaded_photo_arr.each do |arr|
        arr[:user_id] = @user.id.to_s 
      end
      photo_gallery_uploaded_photo_arr
    end

    def modify_product_stickers_params
      product_stickers_arr = []

      product_stickers_attributes.each do |key, value_hash|
        product_stickers_arr << value_hash unless value_hash[:sticker_id].blank?
      end
      product_stickers_arr
    end

    def modify_product_texts_params
      product_texts_arr = []

      product_texts_attributes.each do |key, value_hash|
        product_texts_arr << value_hash unless value_hash[:text_content].blank?
      end
      product_texts_arr
    end

    def set_background_image_params(bg_img_data, bg_params)
      if bg_img_data.split(':')[0] == "data"
        background_data = bg_params.delete(:product_background_data)
        bg_params[:background_image] = generate_background_image(background_data, @new_product.name)
      elsif bg_img_data.is_a? Numeric
        background = Spree::Background.find(bg_img_data)
        bg_params[:background_image] = background.image
      end
      bg_params
    end

    def upload_background_image(product)
      template_bg_image = product.template.background_image
      product_bg_img = product.product_background.background_image

      product.product_background.background_image = template_bg_image if template_bg_image.present? && product_bg_img.blank?
    end

    def upload_frame(product)
      template_frames = product.template.template_frames
      if template_frames.present? && product.product_background.product_frame_id.nil?
        product.product_background.product_frame_id = product.template.template_frames.first.id
      end
    end
end