// // Place all the behaviors and hooks related to the matching controller here.
// // All this logic will automatically be available in application.js.
// // Filter Add
Spree.ready(function() {
  show_hide_gradient();
  hide_show_delete_button();
  $countColorArr = [];

  $(document).on('focus', 'input.filter-color', function(event){
    bind_colorpicker();
  });

  function bind_colorpicker() {
    $('.filter-color').colorpicker();
  }

  $(document).on('change', '.filter-color', function(){
    gradientColor($(this));
  });

  $('#gradientCheck').on('click', function(){
    show_hide_gradient();
  })

  function gradientColor(filterColor){
    var colorAll = "";
    var countColorObj = {};
    countColorObj[`${filterColor.attr('id')}`] = filterColor.colorpicker('getValue', '#ffffff');

    if(_.isEmpty(_.find($countColorArr, filterColor.attr('id')))) {
      $countColorArr.push(countColorObj);
    } else {
      var activeIndex = _.indexOf($countColorArr, _.find($countColorArr, filterColor.attr('id')));
      $countColorArr[activeIndex][filterColor.attr('id')] = filterColor.colorpicker('getValue', '#ffffff');
    }
    colorAll = getColorAll();
    $('.gradient-strip-color').css('background', `linear-gradient(to right${colorAll})`);
    $('.filter-div').css('background', `linear-gradient(to right${colorAll})`);

    filterColor.closest('.input-color-grp').find('.input-group-addon').css("background-color", filterColor.colorpicker('getValue', '#ffffff') );

    if( $('.filter-color').length == 1){
      $('.filter-div').css('background', filterColor.colorpicker('getValue', '#ffffff'));
      filterColor.attr('data-color-pick', filterColor.val());
    }

  }

  function getColorAll(){
    colorAll = ""
    for(var i=0; i <= $countColorArr.length - 1; i++) {
      colorAll = colorAll + ', ' + $countColorArr[i][getKey($countColorArr[i])];
    }

    return colorAll
  }

  function show_hide_gradient() {
    if ($('#gradientCheck').is(':checked')){
      $('.gradientbox').slideDown();
      $('#new_add_color_codes').css({ "display": 'block'} );
    } else {
      $('.gradientbox').slideUp();
      $('#new_add_color_codes').css({ "display": 'none'});
    }
  }

  function getKey(data) {
    for (var prop in data)
      return prop;
  }

  $(document).on('focus', '.spree_remove_fields', function(event){
    var removeInputId = $(this).closest('.color_code').find('.filter-color')[0].id
    var activeIndex = _.indexOf($countColorArr, _.find($countColorArr, removeInputId));
    setTimeout(function(){
      $countColorArr.splice($.inArray($countColorArr[activeIndex], $countColorArr),1);
      if($countColorArr.length == 1){
        $('#gradientCheck').click();
        show_hide_gradient();
        $('.filter-div').css('background', $countColorArr[0][getKey($countColorArr[0])]);
        $('.gradient-strip-color').css('background', 'transparent');
      }
      else{
        colorAll = getColorAll();
        $('.gradient-strip-color').css('background', `linear-gradient(to right${colorAll})`);
        $('.filter-div').css('background', `linear-gradient(to right${colorAll})`);
      }
      if($(".filter-color:visible").length == 1)
      {
        hide_delete_button();
      }
    }, 200);

  });

  function hide_show_delete_button(){
    if($('.spree_remove_fields').length > 1){
      show_delete_button();
    }else{
      hide_delete_button();
    }
  }

  function hide_delete_button(){
    $('.spree_remove_fields').hide();
  }

  function show_delete_button(){
    $('.spree_remove_fields').show();
  }

  $(document).on('click', '.spree_add_fields', function() {
    hide_show_delete_button();
  });

  // index page js
  show_gradient();

  function show_gradient(){
    $('.color-gradient-arr').each(function(i){
      var colorAll = "";
      countColorArr = $($('.color-gradient-arr')[i]).data('color-arr')
      if(countColorArr.length > 1){
        for(var j=0; j <= countColorArr.length - 1; j++) {
          colorAll = colorAll + ', ' + countColorArr[j];
        }
        $($('.color-gradient-arr')[i]).find('.gradient-strip-color').css('background', `linear-gradient(to right${colorAll})`);
      }
      else{
        $($('.color-gradient-arr')[i]).find('.gradient-strip-color').css('background', countColorArr[0]);
      }
   });
  }

  // edit page js
  show_strip_color();

  function show_strip_color(){
    if($('.filter-color').length > 0){

      for(i = $('.filter-color').length - 1; i>=0; i--){
        $($('.filter-color')[i]).closest('.input-color-grp').find('.input-group-addon').css("background-color", $($('.filter-color')[i]).colorpicker('getValue', '#ffffff') );

        gradientColor($($('.filter-color')[i]))
      }

      if($('.filter-color').length > 1 && $('#gradientCheck').length == 1){
        $('#gradientCheck').click();
      }
    }
  }

});
