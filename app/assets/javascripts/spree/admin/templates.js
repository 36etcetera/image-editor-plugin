$(document).ready(function(){
  // Template Add
  var bgImage = $('#template_background_image_field');
  var bgColor = $('#template_background_color_field');

  bgImage.hide();
  bgColor.hide();
  CheckEditFormLoad();
  bindEventToSelectTabs();
  setDefaultValue();
  bindChangeEventOnChangeBackground();
  blankFramePreviewOnDeleteFrame();
  removeImageFromFrames();
  changeBorderRadiusRangeOnChangeDimensions();

  function changeBorderRadiusRangeOnChangeDimensions(){
    $('#template_width').on('blur', function(){
      var width = $(this).val();
      $('#template_edges').attr('max', width);
    });
  }

  //======================  Zoom in & out ================================
  $('.zoom-inout-scale .btn').click(function(e){
      e.preventDefault();
      var attrNature = $(this).attr('data-zoom');
      var currentScale = parseFloat($(this).closest('.zoom-inout-scale').attr('data-zoom-scale'));
      var scaleChange = parseFloat($(this).closest('.zoom-inout-scale').attr('data-zoom-change'));
      var newValue;

      valueChange(scaleChange, this, currentScale, attrNature, newValue);
  })

  //=====================Set value on change input =======================
  $('.form-content input[type="number"]').on('input', function(){
      var switchAttr = $(this).attr('data-range');
      var previewShape = $('.preview-shape');
      var valueSpan = $(this).closest('.form-group').find('.valueShow');
      var prevValue = $(this).val();
      previewShape.css('display', 'block');
      switch(switchAttr){
        case 'width':
        previewShape.css('width', `${prevValue}px`);
        valueSpan.html(`${prevValue}px`);
        $(this).val(prevValue);
        break;

        case 'height':
        previewShape.css('height', `${prevValue}px`);
        valueSpan.html(`${prevValue}px`);
        break;
      }
  });

  $('.form-content input[type="range"]').on('input', function(){
      var switchAttr = $(this).attr('data-range');
      var previewShape = $('.preview-shape');
      var valueSpan = $(this).closest('.form-group').find('.valueShow');
      var prevValue = $(this).val();
      previewShape.css('display', 'block');
      switch(switchAttr){
        case 'radius':
          previewShape.css('border-radius', `${prevValue}px`);
          valueSpan.html(`${prevValue}px`);
        break;

        case 'resolution':
          valueSpan.html(`${prevValue}`);
        break;
      }
  });

//======================  Set background Image ============================
  $('.background-image-block input').on('change', function(){
    readURL(this);
  });

  $('[data-hook="admin_template_form_frame"]').on('change', '.add-template-frames input[type=file]', function(){
    readFrameURL(this);
  });

  function readURL(input) {
    var reader = new FileReader();
    if (input.files && input.files[0]) {
      reader.onload = function(e) {
        $('.preview-shape').css('background-image', `url(${e.target.result})`);
        $('.preview-shape').css('background-size', '100% 100%');
        $('.preview-shape').css('background-color', 'transparent');
        $('#template_background_color').val('');
        $('#template_background_color_field .color').css('background-color', '#fff');
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readFrameURL(input) {
    var reader = new FileReader();
    if (input.files && input.files[0]) {
      reader.onload = function(e) {
        var framelayer = $(input).attr('data-layer');
        if(framelayer == undefined){
          framelayer = "frame-layer-1";
        }
        if(framelayer == "frame-layer-1"){
          $('#frame-preview img').attr('src', e.target.result);
        }
        var selectedlayer = $(".frame-images[data-layer='"+ framelayer +"']");
        $(input).closest('.template-frames').find('.frame-img-input').attr('src', e.target.result)

        if(selectedlayer.length > 0){
          selectedlayer.find('img').attr('src', e.target.result);
        }
        else{
          $('.frame-list').prepend(`<div class="frame-images" data-layer="${framelayer}"><img src="${e.target.result}"></div>`)
        }
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
//=============Setting for background color using color picker=============

  $('#template_background_color_field .colorpicker-element-bg').colorpicker().on('change', function(){
    $('.preview-shape').css('background-image', `none`);
    $('.preview-shape').css('background-color', $(this).colorpicker('getValue', '#ffffff'));
    $('#template_background_image').val('');
    var $el = $('#template_background_image');
    $el.wrap('<form>').closest('form').get(0).reset();
    $el.unwrap();
  })

  // layer settings admin
  $('#layer-setting-admin a').on('click', function(){
    var currentValue = $(this).attr('data-value');
    var settingPanel = $('.layer-settings');
    var buttonHtm = $('.layer-dropdown-admin .dropdown-toggle');
    buttonHtm.html(currentValue);
    settingPanel.removeClass('disabled');
    settingPanel.attr('data-layer', currentValue);
    $('.template-preview').removeClass('sticky-bottom');
  });

  //==========Set value and preview for template associated fields ========
  $('.layer-settings input').on('input', function(){
    //for template images
    var activeVal = $(this).val();
    var activeAttr = $(this).attr('data-effect');
    var activeLayer = $(this).closest('.layer-settings').attr('data-layer');
    var activeLayerCtrl = $('#layer-setting-admin').find(`[data-value=${activeLayer}]`);
    var docLayer = $('.preview-shape').find(`[data-layer=${activeLayer}]`);
    var valueSpan = $(this).closest('.form-group').find('.valueShow');
    var activeStyle = $(this).attr('data-style');
    var textContent = docLayer.find('.text-content');

    activeLayerCtrl.attr(`data-${activeAttr}`, activeVal);
    valueSpan.html(`${activeVal}px`);
      switch(activeAttr){
          case 'width':
          docLayer.css('width', `${activeVal}px`);
          docLayer.css('background', `#ffffff`);
          break;
          case 'height':
          docLayer.css('height', `${activeVal}px`);
          docLayer.css('background', `#ffffff`);
          break;
          case 'bradius':
            docLayer.css('border-radius', `${activeVal}px`);
          break;
          case 'positionX':
          docLayer.css('left', `${activeVal}px`);
          break;
          case 'positionY':
          docLayer.css('top', `${activeVal}px`);
          break;
          case 'rotate':
          docLayer.css('transform', `rotate(${activeVal}Deg)`);
          valueSpan.html(`${activeVal}Deg`);
          break;
      }

      switch(activeStyle){
          case 'width':
          docLayer.css('width', `${activeVal}px`);
          textContent.css('width', `${activeVal}px`);
          break;
          case 'height':
          docLayer.css('height', `${activeVal}px`);
          textContent.css('height', `${activeVal}px`);
          break;
          case 'positionX':
          docLayer.css('left', `${activeVal}px`);
          break;
          case 'positionY':
          docLayer.css('top', `${activeVal}px`);
          break;
          case 'rotate':
          docLayer.css('transform', `rotate(${activeVal}Deg)`);
          valueSpan.html(`${activeVal}Deg`);
          break;
          case 'font_size':
          textContent.css('font-size', `${activeVal}px`);
          break;
      }
  });

  function layerSwitchDetails(layerPanel, defaultValue, dataEffect, $this){
      var activeDataInput = layerPanel.find(`[data-effect="${dataEffect}"]`);
      var activeEffectPresent = $($this).attr(`data-${dataEffect}`);
      var valueSpan = activeDataInput.closest('.form-group').find('.valueShow');
      if(typeof activeEffectPresent !== 'undefined') {
          activeDataInput.val(activeEffectPresent);
          valueSpan.html(`${activeEffectPresent}px`);
      } else {
          activeDataInput.val(defaultValue);
          valueSpan.html(`${defaultValue}px`);
      }
  }

  function setDefaultValue() {
    if ($('.new_template').length > 0){
      $('.form-content input[type="range"]').each(function(){
        $(this).val(0);
      })
    }
    var attrNature = "negative";
    var currentScale = parseFloat($('.zoom-inout-scale').attr('data-zoom-scale'));
    var newValue = parseFloat($('.zoom-inout-scale').attr('data-zoom-scale'));
    currentScale = currentScale == 0  ? 0.1 : currentScale
    newValue = newValue == 0  ? 0.1 : newValue
    valueChange(0.0, $('.zoom-inout-scale .btn'), currentScale, attrNature, newValue);
  }

//================= Load preview content after edit  ======================
  function CheckEditFormLoad(){
    if($('.edit_template').length > 0){
      setTimeout(function(){

        var textLayerObj = $('.txt-lyr');
        var imgUrl = $('.background-image-block input').attr('data-url');
        var bgColorValue = $('#template_background_color').attr('value');

        $('.font').trigger('change');
        $('.colorpicker-element').trigger('change');
        $('.form-content input[type="number"]').trigger('input');
        $('.form-content input[type="range"]').trigger('input');

        $.each( textLayerObj, function(index, value) {
          addDummyTextOnPreview(value);
          previewSetTextContent(value);
        });

        $.each( $('.font'), function(index, value) {
          bindEventToSelectFontFamily(value);
        });

        $('#template_background_color_field .colorpicker-element-bg').val(bgColorValue);
        $('#template_background_color_field .colorpicker-element-bg').trigger('change');

        if (imgUrl.length > 0){
          $('.preview-shape').css('background-image', 'url(' + imgUrl + ')');
        }

        $('.preview-shape').css('background-size', '100% 100%');

        $('.layer-settings input').trigger('input');

        if($('#template_background_color').attr('value').length > 0){
          $('#template_background_background_color').prop('checked', true);
          bgColor.show();
        }
        else{
          $('#template_background_background_image').prop('checked', true);
          bgImage.show();
        }
      }, 100);
    }
  }

//============================ Validations on tabs==========================
  show_tab_error();
  function show_tab_error(){
    layerArray=[]

    if($('.formError').length){
      $('.formError').each(function(i){
        linkHref= $($('.formError')[i]).closest('.tab-pane').attr('id')

        if(jQuery.inArray(linkHref, layerArray) == -1)
        {
          layerArray.push(linkHref)
        }
      });
      $.each(layerArray, function( index, value ) {
        $('a[href$="'+value+'"]').css('background', '#f57e80');
        var parentTab = $('a[href$="'+value+'"]').closest('.tab-pane').attr('id');
        $('a[href$="#'+parentTab+'"]').css('background', '#f57e80');
      });
    }
  }

  var numberOfImages = $('#template_number_of_images').val();
  var numberOfBuildLayers = $('#layer-setting-admin li').length;

  if (numberOfImages == numberOfBuildLayers){
    $('#errorExplanation')
  }

//==========================================================================

  $(function (){
    $('.font').fontselect().change(function(){
      currentElement = $(this);
      bindEventToSelectFontFamily(currentElement);
    });
  });

  $('.colorpicker-element').colorpicker().on('change', function(){
    $(this).closest('li').find('.color').css("background-color", $(this).colorpicker('getValue', '#ffffff') );
    $(`.preview-inner-img[data-layer="${$(this).closest('.layer-settings').attr('data-layer')}"]`).css('color', $(this).colorpicker('getValue', '#ffffff'))
  });

  //==============Preview frame=======================
  $('.frame-list').on('mouseenter', 'img', function(){
    var Img = $(this);
    var frameImage = $(Img).clone();
    $('#frame-preview').html('');
    $('#frame-preview').append(frameImage);

    var selectedAttrLayer = Img.closest('.frame-images').attr('data-layer');
    var selectedFrameLayer = $('.template-frames').find('.form-control[data-layer="'+selectedAttrLayer+'"]');
    selectedFrameLayer.focus();
  });

  bindClickEventOnTextsLayerTab();

  //==============Add text content by admin and save==============
  $(document).on('blur', '.text-content', function(){
    getAndSetTextContentValue($(this))
  });
});

getAndSetTextContentValue = function(dom){
  var dataLayer = $(dom).closest('.preview-inner-img').attr('data-layer');
  var templateTextLayerObj = getTemplateTextLayer(dataLayer);
  var InputTextContentObj = getTemplateTextLayerContentField(templateTextLayerObj);
  var content = $(dom).text().trim();
  InputTextContentObj.val(content);
}

getTemplateTextLayer = function(targetLayer){
  return $('.layer-settings[data-layer="'+ targetLayer +'"]');
}

getTemplateTextLayerContentField = function(templateTextLayerObj){
  return templateTextLayerObj.find($('input[type=hidden][data-style="text-content"]'));
}
//===========================================================================
function bindClickEventOnTextsLayerTab(){
  $('.txt-lyr').click(function(){
    var currentElement = $(this)
    addDummyTextOnPreview(currentElement);
  });
}

function addDummyTextOnPreview(currentElement) {
  var dataAttr = $(currentElement).attr('data-value');
  if ($(`.preview-inner-img[data-layer="${dataAttr}"]`).children('.layer-doc').length == 0) {
    $(`.preview-inner-img[data-layer="${dataAttr}"]`).append(`<div class="layer-doc layer-text" text-layer="${dataAttr}">
              <div class="text-content text-appended" spellcheck=false contenteditable="true">
              This is dummy text
              </div>
              </div>`);
  }
}

function bindEventToSelectFontFamily(currentElement){
  var font = $(currentElement).val().replace(/\+/g, ' ');
  font = font.split(':');
  $('p').css('font-family', font[0]);
  var activeStyle = $(currentElement).closest('.form-group').find('.font').attr('data-style');
  var activeLayer = $(currentElement).closest('.layer-settings').attr('data-layer');
  var activeLayerCtrl = $('#layer-setting-admin').find(`[data-value=${activeLayer}]`);
  var docLayer = $('.preview-shape').find(`[data-layer=${activeLayer}]`);
  var textContent = docLayer.find('.text-content');
  activeLayerCtrl.attr(`data-${activeStyle}`, font[0]);
  $(textContent).css('font-family', font[0]);
}

//====================Select Tabs===========================
function bindEventToSelectTabs(){
  $('#interest_tabs').on('click', 'a[data-toggle="tab"]', function(e) {
    e.preventDefault();

    var $link = $(this);

    if (!$link.parent().hasClass('active')) {

      //remove active class from other tab-panes
      $('.tab-content:not(.' + $link.attr('href').replace('#','') + ') .tab-pane').removeClass('active');

      // click first submenu tab for active section
      $('a[href="' + $link.attr('href') + '_all"][data-toggle="tab"]').click();

      // activate tab-pane for active section
      $('.tab-content.' + $link.attr('href').replace('#','') + ' .tab-pane:first').addClass('active');
    }

  });
}

function bindChangeEventOnChangeBackground(){
  $('.form-content input[type="radio"]').on('change', function(){
    var templateBgImage = $('#template_background_background_image').is(':checked');
    var templateBgColor = $('#template_background_background_color').is(':checked')
    if (templateBgImage == true) { bgImage.show(); }
    else{ bgImage.hide(); }

    if (templateBgColor == true) {
      bgColor.show();
      $('#template_remove_background_image').prop('checked', true)
    }
    else{ bgColor.hide(); }
  });
}

// ============== Zoom-In & Zoom-Out Document Value Change =============

function valueChange(valueOperator, $this, currentScale, attrNature, newValue){
    if(currentScale < 0.3 && attrNature == "negative" || currentScale > 1.5 && attrNature == "positive") {
        newValue = currentScale;
    } else {
      if (attrNature == "negative") {
        newValue = currentScale - valueOperator;
      } else {
        newValue = currentScale + valueOperator;
      }

    }
    if(newValue.toFixed(2) == 1.00) {
        $('.current-size').html('Scale: Original')
    } else {
        $('.current-size').html(`Scale: ${newValue.toFixed(2)}`)
    }
    $('.preview-shape').css('transform', `scale(${newValue})`);
    setTimeout(function(){
      var width = $('.preview-shape').get(0).getBoundingClientRect().width;
      var height = $('.preview-shape').get(0).getBoundingClientRect().height;
      $('.scale-parent').css('width', width + 'px');
      $('.scale-parent').css('height', height + 'px');
    }, 1000);
    $($this).closest('.zoom-inout-scale').attr('data-zoom-scale', `${newValue}`);
}
//============ For text content preview on edit page =================

previewSetTextContent = function(target){
  var layer = $(target).attr('data-value');
  var templateTextLayer = getTemplateTextLayer(layer);
  var inputTextContentField = getTemplateTextLayerContentField(templateTextLayer);
  var textContentLayer = $('.preview-inner-img[data-layer="'+layer+'"]').find('.text-content');
  var textContentFieldValue = inputTextContentField.val();
  textContentLayer.html(textContentFieldValue);
}

function blankFramePreviewOnDeleteFrame(){
  if($('.delete-frame').length == 0){
    $('#frame-preview').html('');
  }
}

function removeImageFromFrames(){
  $('.spree_add_frame_fields').click(function(){
    var lastTemplateFrame = $('.add-template-frames').find('tr:visible:first');
    var framesLength = $('.add-template-frames').find('tr').length;
    lastTemplateFrame.find('img').remove();
    lastTemplateFrame.find('.actions a').removeAttr('url style');
    lastTemplateFrame.find('.actions a').removeClass('delete-frame');
    lastTemplateFrame.find('input[type="file"]').attr('data-layer', 'frame-layer-'+framesLength);
  });
}
