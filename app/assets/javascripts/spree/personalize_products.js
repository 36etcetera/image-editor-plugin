//= require spree/frontend/custom/defined_variables.js
//= require spree/frontend/custom/common_functions.js
//= require spree/frontend/custom/add_layers_to_document.js
//= require spree/frontend/custom/assign_values_to_product_image_layers.js
//= require spree/frontend/custom/assign_values_to_product_sticker_layers.js
//= require spree/frontend/custom/assign_values_to_product_text_layers.js
//= require spree/frontend/custom/draggable_and_resizable_layer.js
//= require spree/frontend/custom/image_drag_and_lock.js
//= require spree/frontend/custom/layer_settings.js
//= require spree/frontend/custom/navbar_layer_panel_open.js
//= require spree/frontend/custom/preview_image.js
//= require spree/frontend/custom/product_text_features.js
//= require spree/frontend/custom/remove_or_select_layer.js
//= require spree/frontend/custom/select_layer_dropdown_and_its_properties.js
//= require spree/frontend/custom/sidebar_panel.js
//= require spree/frontend/custom/upload_gallery_photos.js
//= require spree/frontend/custom/upload_image.js
//= require spree/frontend/custom/zoom_in_and_out.js

$(document).ready(function(){
    colorpickerSpectrum();
    colorpickerSpectrumBackground();
    $('.draggable-only').draggable();
    $('.owl-carousel-2').owlCarousel({
        loop:false,
        margin:10,
        items: 1,
        nav:true,
        navText: [
            '<i class="fas fa-angle-left"></i>',
            '<i class="fas fa-angle-right"></i>'
        ]
    }) 
    $('.owl-carousel').owlCarousel({
        loop:false,
        margin:10,
        nav:true,
        autoWidth: true,
        navText: [
            '<i class="fas fa-angle-left"></i>',
            '<i class="fas fa-angle-right"></i>'
        ]
    })

    $('.main-content').on('click', function(){
        hvrItemsSlideUp();
    });

    disableRightClickForImage();

    $('.scale-doc-section').css('transform', 'scale(1)');
    bindClassOnProductLayers();
    bindLayerOnProductGalleryLayers();
    $(function (){
        $('.font').fontselect();
        $('.font-select').hide();
    });
    $('.google-font').fontselect();
    $(function(){
      $('.google-font').fontselect().change(function(){

        // replace + signs with spaces for css
        var font = $(this).val().replace(/\+/g, ' ');

        // split font into family and weight
        font = font.split(':');

        // set family on paragraphs
        $('p').css('font-family', font[0]);
      });
    });
    if($('.bg-image').attr('src') == 0){
       $('.bg-image').css('display', 'none');
    }
    fitToScreen();
    saveSelectedGalleryPhoto(); 
    bindClickEventOnAddGalleryPhotosBtn();
    removeBackgroundImage();
    addClassActiveToFirstImageLayer();
    AssignValueToCustomWidth();
});

removeBackgroundImage = function(){
  $('.remove_background_image').click(function(){
    if($('.remove_background_image:checkbox:checked').length == 1){
      mainImglayerImg.attr('src', '');
      mainImglayerImg.css('opacity', '0');
    }
  })
}

AssignValueToCustomWidth = function(){
    var cardBodyCustomImage = $('.card-body[data-effect="cusImage"]').find('.layer-dropdown-container li');
    $.each(cardBodyCustomImage, function(index, value) {
        var dataLayer = $(value).attr('data-layer-doc');
        var customWidth = $(value).attr('data-imgwidth');
        var panelLayer = getSelectedImagesLayerPanel(dataLayer);
        panelLayer.find('.layer-scale .value-span-input').text(customWidth);
    });
}

addClassActiveToFirstImageLayer = function(){
  $('.layer-images-panel[id="layer-1"]').addClass('active');
}

// Layer Active
$('.main-img-layer').on('click', function(){
    activeLayerDisabled();
});

$('.scale-doc-section').delegate('.layer-doc', 'click', function(){
    $('.layer-doc').removeClass('active-layer');
    activeLayerDisabled();
    $(this).addClass('active-layer');
    $('.sidebar-toggle').addClass('active');
});

var progressBar = $('.progress-bar');
   
$('form.new_personalize_product').ajaxForm({
    beforeSend: function() {
      $('.loader-bg').removeClass('d-none');
      var percentVal = '0%';
      progressBar.width(percentVal)
      progressBar.html(percentVal);
    },
    uploadProgress: function(event, position, total, percentComplete) {
      var percentVal = percentComplete + '%';
      progressBar.width(percentVal)
      progressBar.html(percentVal);
    },
    success: function() {
      var percentVal = '100%';
      progressBar.width(percentVal)
      progressBar.html(percentVal);
    },
  complete: function(xhr) {
    console.log('Complete');
  }
}); 

$('.layer-set h3').click(function(){
    $('.layer-set').removeClass('active-layer-effect');
    $(this).closest('.layer-settings').addClass('effect-active');
    $(this).closest('.layer-set').addClass('active-layer-effect');
});

$('.btn-back-setting').click(function(){
    $('.layer-set').removeClass('active-layer-effect');
    $('.layer-settings').removeClass('effect-active');
});

// Instructions
$('#disable-instructions').click(function(){
    $(".instruction-new").remove();
});

// Start Screen Window
$('.btn-toggle').click(function(){
    $('.select-links').addClass('active');
    $(this).closest('.select-links').removeClass('active');
})

activeLayerDisabled = function(){
    $('.layer-doc').removeClass('active-layer');
    $('.layer-text-drag').draggable({
        disabled:false
    })
}

disableRightClickForImage = function(){
  jQuery(function() {
    jQuery(this).bind("contextmenu", function(event) {
        event.preventDefault();
    });
  });
}
