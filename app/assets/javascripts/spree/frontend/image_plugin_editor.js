// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// require_tree .

//Jquery 
//= require jquery
//= require spree/frontend/js/jquery.mousewheel.min.js
//= require jquery_ujs
//= require pnotify

//Bootstrap
//= require spree/frontend/bootstrap/bootstrap-colorpicker.js

//Justify
//= require spree/frontend/justified/jquery.justifiedGallery.js
//= require spree/frontend/justified/jquery.justifiedGallery.min.js

//Framework
//= require spree/frontend/js/jquery.form.js
//= require spree/frontend/js/html2canvas.min.js
//= require spree/frontend/js/Popper.js
//= require spree/frontend/bootstrap/bootstrap.min.js
//= require spree/frontend/js/jquery-ui.min.js
//= require spree/frontend/js/jquery.ui.touch-punch.min.js
//= require spree/personalize_products.js
//= require spree/backend/js/jquery.fontselect.js

// Owl Carousel
//= require spree/frontend/owl_carousel/owl.carousel.min.js

//I18n
//= require i18n
//= require i18n.js
//= require i18n/translations

//Colorpicker Spetrum
//= require spree/frontend/js/spectrum.js