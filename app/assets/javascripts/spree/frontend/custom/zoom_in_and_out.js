$(window).resize(function () { 
    fitToScreen();
});

// Zoom in & out
$('.doc-scale .btn').click(function(e){
    e.preventDefault();
    var attrNature = $(this).attr('data-zoom');
    var currentScale = parseFloat($(this).closest('.doc-scale').attr('data-current-scale'));
    var newValue;

    switch(attrNature){
        case "positive":
        var positiveScale = currentScale * (20/100);
        valueChange(+positiveScale, this, currentScale, attrNature, newValue);
        break;

        case "negative":
        var negativeScale = currentScale * (20/100);
        valueChange(-negativeScale, this, currentScale, attrNature, newValue);
        break;
    }
})

// ============== Zoom-In & Zoom-Out Document Value Change =============

function valueChange(valueOperator, $this, currentScale, attrNature, newValue){
    if(currentScale < 0.05 && attrNature == "negative" || currentScale > 1.5 && attrNature == "positive") {
        newValue = currentScale;
    } else {
        newValue = currentScale + valueOperator;
    }
    if(newValue == 1) {
        $('.current-size').html('Scale: Original')
    } else {
        $('.current-size').html("Scale: " + newValue.toFixed(2))
    }
    scaleDocSection.css('transform', 'scale(' + newValue + ')').promise().done(function(){
        setTimeout(function(){
            var afterScaleHeight = scaleDocSection.get(0).getBoundingClientRect().height;
            var afterScaleWidth = scaleDocSection.get(0).getBoundingClientRect().width;
            newDocument.css('width', afterScaleWidth + 'px');
            newDocument.css('height', afterScaleHeight + 'px');
        }, 400)
    });

    $($this).closest('.doc-scale').attr('data-current-scale', newValue);
}
//===========Fit to screen and fit to original====================

function fitToScreen(){
    var documentHeight = $('.main-content').height();
    var templateHeight = scaleDocSection.height();
    var documentWidth = $('.main-content').width();
    var templateWidth = scaleDocSection.width();

    var heightDifference = documentHeight / templateHeight;
    var widthDifference = documentWidth / templateWidth;

    scaleCheck(heightDifference).promise().done(function(){
        setTimeout(function(){
            if($('.main-content').get(0).scrollWidth > window.innerWidth || $('.main-content').get(0).scrollHeight > window.innerHeight) {
                scaleCheck(widthDifference);
            }
        }, 1000);
    })
}
function scaleCheck(scaleValue){
    $('.doc-scale').attr('data-current-scale', scaleValue);
    scaleDocSection.css('transform-origin', '0 0 0');
    return scaleDocSection.css('transform', "scale(" + scaleValue + ")").promise().done(function(){
        setTimeout(function(){
            var afterScaleHeight = scaleDocSection.get(0).getBoundingClientRect().height;
            var afterScaleWidth = scaleDocSection.get(0).getBoundingClientRect().width;
            newDocument.css('width', afterScaleWidth + "px");
            newDocument.css('height', afterScaleHeight + "px");
        }, 600);
    });
}
