//====================  Remove layer =========================
$('.layer-details-panel .btn-remove').on('click', function(){
    $this = $(this)
    var activeAttribute = getLayerAttr($this);
    var layerpanel = $(this).closest('.layer-details-panel');
    var dropdownpanel = getLayerDropDownContainer(activeAttribute);
    var activeLayerpanel = getScaleDocSection(activeAttribute);
    var activeLayerGroup = $('.card').find("[data-layer-doc=" + activeAttribute + "]").closest('.collapse').get(0).id;
    var layerEffectToggleBtn = $(".sidebar-collapsed-icons li[data-target='#" + activeLayerGroup + "']");
    var selectedCard = layerpanel.closest('.card-body');
    var productSticker = productStickerLayerObjForLayerName(activeAttribute);
    var productText = productTextLayerObjForLayerName(activeAttribute);
    layerEffectToggleBtn.trigger('click');

    activeLayerpanel.remove();
    dropdownpanel.remove();
    if(productSticker.length > 0){
        productSticker.remove();
    }
    if(productText.length > 0){
        productText.remove();
    }
    layerpanel.attr('data-layer-doc', '');
    selectedCard.find('.dropdown-toggle').html('Please Select');
    $('.layer-details-panel').addClass('disabled');
});

//===============  Layer Panel Open on selecting layer ================
$('.scale-doc-section').delegate('.layer-doc', 'click', function(){
    var attr = $(this).attr('data-layer-doc');
    var activeLayerGroup = $('.card').find("[data-layer-doc=" + attr + "]").closest('.collapse').get(0).id;
    var layerEffectToggleBtn = $(".sidebar-collapsed-icons li[data-target='#" + activeLayerGroup + "']");
    if(layerEffectToggleBtn.hasClass('active')) {
        $(".layer-dropdown li[data-layer-doc='" + attr + "']").trigger('click');
    } else {
        layerEffectToggleBtn.trigger('click');
        $(".layer-dropdown li[data-layer-doc='" + attr + "']").trigger('click');
    }
});