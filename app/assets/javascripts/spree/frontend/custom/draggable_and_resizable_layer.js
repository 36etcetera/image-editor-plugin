// Dragable & Resizable
$('.draggableonly')
    .draggable({
        start: function (event, ui) {
            var left = parseInt($(this).css('left'),10);
            left = isNaN(left) ? 0 : left;
            var top = parseInt($(this).css('top'),10);
            top = isNaN(top) ? 0 : top;
            recoupLeft = left - ui.position.left;
            recoupTop = top - ui.position.top;
        },
        drag: function (event, ui) {
            ui.position.left += recoupLeft;
            ui.position.top += recoupTop;
        }
    });

// On Selecting text Layer Dragging Disabled
$('.scale-doc-section').delegate('.layer-text-drag', 'click', function(){
    $(this).draggable({
        disabled: true
    });
    $(this).find('.text-content').focus();
 });