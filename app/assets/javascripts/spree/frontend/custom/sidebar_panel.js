// ========== Sidebar Collapsed Panel====================
$('.sidebar-collapsed-icons li').click(function(){
    $('.sidebar-collapsed-icons li').css('pointer-events', 'none');
    var condition = $(this).hasClass('active');
    $('.sidebar').addClass('sidebar-active');
    $('.main-layout').addClass('sb-active');
    $('.card').css('display', 'none');
    $($(this).attr('data-target')).closest('.card').css('display', 'block');
    $('.sidebar-collapsed-icons li').removeClass('active');
    setTimeout(function(){ $('.sidebar-collapsed-icons li').css('pointer-events', 'auto') }, 1000);
    if(condition) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active')
    }
});

// Sidebar
$('.btn-toggle-sb').click(function(){
    $('.sidebar').toggleClass('sidebar-active');
    $('.main-layout').toggleClass('sb-active');
})

$('.sidebar-toggle').click(function() {
    $(this).toggleClass('active');
    $('.main-layout').toggleClass('sb-active');
    $('.sidebar').toggleClass('sidebar-active');
})