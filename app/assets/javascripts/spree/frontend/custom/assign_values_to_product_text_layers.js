//=================Set value in Product Text Layer====================//
$('.item-hvr[data-effect="text"]').click(function(){
    $('.spree_add_texts_fields').trigger('click');
    $('.product-texts').removeAttr('data-layer-doc');
    bindClassOnProductLayers();
});

//================= Override spree_add_fields function ================//
$('.spree_add_texts_fields').click(function() {
  var templateTextsLength = $('.product-texts').length;
  var productLastText = $('.product-texts:last');
  bindFieldsForProductProperties($(this), templateTextsLength, productLastText);
})

function EventToPassValueInProductTexts(){
    var CustomTextLayers = $('.card-body[data-effect="text"]').find('.layer-dropdown-container li');
    $.each( CustomTextLayers, function(index, value) {
        var textLayerId = $(value).attr('data-layer-doc');
        var textLayerText = $(".layer-text[data-layer-doc=" + textLayerId + "]");
        var textLayerContent = textLayerText.find('.text-content').html();
        var textLayerRotate = $(value).attr('data-rotate');
        var textLayerLineHeight = $(value).attr('data-lineheight');
        var textLayerFontSize = $(value).attr('data-fontsize');
        var textLayerFontFamily = $(value).attr('data-value');
        var textLayerOpacity = $(value).attr('data-opacity');
        var textLayerAlign = $(value).attr('data-textalign');
        var textLayerfontColor = $(value).attr('data-fontcolor');

        var productTextLayerObj = productTextLayerObjForLayerName(textLayerId);

        var InputIdObj = productTextLayerObj.find($('input[type=hidden][data-effect="text_layer"]'));
        var InputObjTextContent = productTextLayerObj.find($('input[type=hidden][data-effect="text_content"]'));
        var InputObjRotate = productTextLayerObj.find($('input[type=hidden][data-effect="rotate"]'));
        var InputObjLineHeight = productTextLayerObj.find($('input[type=hidden][data-effect="line_height"]'));
        var InputObjFontSize = productTextLayerObj.find($('input[type=hidden][data-effect="font_size"]'));
        var InputObjFontFamily = productTextLayerObj.find($('input[type=hidden][data-effect="font_family"]'));
        var InputObjOpacity = productTextLayerObj.find($('input[type=hidden][data-effect="opacity"]'));
        var InputObjAlignment = productTextLayerObj.find($('input[type=hidden][data-effect="alignment"]'));
        var InputObjStyle = productTextLayerObj.find($('input[type=file][data-effect="style"]'));
        var InputObjFontColor = productTextLayerObj.find($('input[type=hidden][data-effect="font_color"]'));
        
        textLayerOpacity = (textLayerOpacity == undefined) ? '1' : textLayerOpacity;
        textLayerFontSize = (textLayerFontSize == undefined) ? '76' : textLayerFontSize

        InputIdObj.val(textLayerId);
        InputObjTextContent.val(textLayerContent);
        InputObjRotate.val(textLayerRotate);
        InputObjLineHeight.val(textLayerLineHeight);
        InputObjFontSize.val(textLayerFontSize);
        InputObjFontFamily.val(getTextLayerFontFamily(textLayerFontFamily, textLayerId));
        InputObjOpacity.val(textLayerOpacity);
        InputObjAlignment.val(textLayerAlign);
        InputObjFontColor.val(getTextLayerColor(textLayerfontColor, textLayerId));
    });
}

getStyleOfPredefinedTextLayer = function(targetAttrVal){
    return $(".layer-doc[data-layer-doc='" + targetAttrVal + "']").attr('style');
}

getTextLayerFontFamily = function(selectedTextStyle, targetAttrVal){
    if(selectedTextStyle == undefined){
        var getFontFamilyFromString = getStyleOfPredefinedTextLayer(targetAttrVal).split("font-family:")[1]
        if(getFontFamilyFromString == undefined){
            getFontFamilyFromString = 'Times New Roman'
        }
        else {
          getFontFamilyFromString = getFontFamilyFromString.split("!")[0];
        }
        return getFontFamilyFromString;
    }
    else {
        return selectedTextStyle;
    }
}

getTextLayerColor = function(selectedTextStyle, targetAttrVal){
    if(selectedTextStyle == undefined){
        var getTextColorFromString = getStyleOfPredefinedTextLayer(targetAttrVal).split("color:")[1]
        if(getTextColorFromString == undefined){
            getTextColorFromString = 'rgb(39,39,39)';
        }
        else { 
            getTextColorFromString = getTextColorFromString.split("!")[0];
        }
        return getTextColorFromString;
    }
    else{
        return selectedTextStyle;
    }
}
