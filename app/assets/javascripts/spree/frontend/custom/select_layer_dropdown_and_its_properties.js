// Layer Select Dropdown
$('.layer-dropdown .layer-dropdown-toggle').delegate('li', 'click', function(){
    $('.layer-dropdown .layer-dropdown-toggle li').removeClass('active');
    $(this).addClass('active');
    var attrFind = $(this).attr('data-layer-doc');
    var layerPanel = $(this).closest('.layer-dropdown').find('.layer-details-panel#' + attrFind);
    if(layerPanel.length == 0){
        layerPanel = $(this).closest('.layer-dropdown').find('.layer-details-panel');
    }
    else{
        $('.layer-details-panel').removeClass('active');
        layerPanel.addClass('active');
    }
    var layerName = $(this).find('.layer-left').html();
    var layerImage = $(this).find('.layer-right img').attr('src');

    var layerDocFind = getScaleDocSection(attrFind);
    var layerImgWidth = $(this).attr('data-image-width');

    // Active & Non Active Switch Classes In Document
    $('.layer-doc').removeClass('active-layer');
    layerDocFind.addClass('active-layer');
    // --End--

    layerPanel.removeClass('disabled');
    layerPanel.attr('data-layer-doc', attrFind);
    layerPanel.find('.layer-name').html(layerName);
    layerPanel.find('.layer-img').html("<img src=\"" + layerImage + "\" />");

    // Adding Active Layer Name to the Dropdown Button
    $(this).closest('.layer-dropdown-toggle').find('.dropdown-toggle').html(layerName);

    // Layer Input Value Switch
    layerSwitchDetails('0', '.layer-rotate', 'data-rotate', this, layerPanel, 'input');
    layerSwitchDetails('100', '.layer-opacity', 'data-opacity', this, layerPanel, 'input');
    layerSwitchDetails('76', '.layer-fontsize', 'data-fontsize', this, layerPanel, 'input');
    layerSwitchDetails('1', '.layer-lineheight', 'data-lineheight', this, layerPanel, 'input');
    layerSwitchDetails('left', '.layer-textproperties', 'data-textalign', this, layerPanel, 'input[data-style="textalign"]');
    layerSwitchDetails('', '.layer-fontstyle', 'data-fontfamily', this, layerPanel, 'input[data-style="fontfamily"]');

    layerSwitchDetails('normal', '.layer-textproperties', 'data-fontitalic', this, layerPanel, 'input[data-style="fontitalic"]');
    layerSwitchDetails('normal', '.layer-textproperties', 'data-fontweight', this, layerPanel, 'input[data-style="fontweight"]');
    layerSwitchDetails('none', '.layer-textproperties', 'data-fontunderline', this, layerPanel, 'input[data-style="fontunderline"]');
    layerSwitchDetails('#333', '.layer-textproperties', 'data-fontcolor', this, layerPanel, 'input[data-style="fontcolor"]');

    layerSwitchDetails(layerImgWidth, '.layer-scale', 'data-imgwidth', this, layerPanel, 'input[data-style="scale"]');
    layerSwitchDetails('Default', '.layer-pic', 'data-picture-name', this, layerPanel, 'input');

    // Font Family Dropdown Active/Non Active
    activeDropdownTextProperties('data-fontfamily', '.font-dropdown', 'data-value', this);
    activeDropdownTextProperties('data-textalign', '.text-properties', 'data-aligntext', this);

    activeDropdownFontStyle('data-fontitalic', '.text-styles', 'data-textstyle', this, 'italic');
    activeDropdownFontStyle('data-fontweight', '.text-styles', 'data-textstyle', this, 'bold');
    activeDropdownFontStyle('data-fontunderline', '.text-styles', 'data-textstyle', this, 'underline');

    colorSwitch('data-fontcolor', '.text-styles', 'data-textstyle', this);
})

// ============== Layer Switch Value Change In Layer Settings =============
function layerSwitchDetails(inputValue, layerClass, laterAttribute, $this, layerPanelParam, inputSelector) {
    if(typeof $($this).attr(laterAttribute) !== 'undefined'){
        laterAttribute !== 'data-picture-name' ? layerPanelParam.find(layerClass).find(inputSelector).val($($this).attr(laterAttribute)) : null;
        layerPanelParam.find(layerClass).find('.value-span-input').html($($this).attr(laterAttribute));
    } else {
        laterAttribute !== 'data-picture-name' ? layerPanelParam.find(layerClass).find(inputSelector).val(inputValue) : null;
        layerPanelParam.find(layerClass).find('.value-span-input').html(inputValue);
    }
}

function activeDropdownFontStyle(activeDataAttr, layerDropdown, liAttribute, $this, laterAttributeName){
    var activeLayerFind = $($this).attr(activeDataAttr);
    if(typeof activeLayerFind !== 'undefined' && activeLayerFind == laterAttributeName) {
        $(layerDropdown).find("[" + liAttribute + "=\"" + activeLayerFind + "\"]").addClass('active');
    } else {
        $(layerDropdown).find("[" + liAttribute + "=\"" + laterAttributeName + "\"]").removeClass('active');
    }
}

function colorSwitch(activeDataAttr, layerDropdown, liAttribute, $this){
    var activeColor = $($this).attr(activeDataAttr);
    var activeSpanWithColor = $(layerDropdown).find("[" + liAttribute + "=\"color\"]").find('span');
    if(typeof activeColor === 'undefined') {
        activeSpanWithColor.css('background-color', '#333');
    } else {
        activeSpanWithColor.css('background-color', activeColor);
    }
}

$('.layer-dropdown').delegate('li .fa', 'click', function(){
    if($(this).hasClass('fa-eye')) {
        $(this).removeClass('fa-eye')
        $(this).addClass('fa-eye-slash');
    } else {
        $(this).addClass('fa-eye');
        $(this).removeClass('fa-eye-slash');
    }

    var findLayer = $(this).closest('li').attr('data-layer-doc');
    var findDocLayer = getScaleDocSection(findLayer);
    findDocLayer.toggleClass('layer-hidden');
})


// ============== Text Properties(FontStyle & TextAlign) Change =============
function activeDropdownTextProperties(activeDataAttr, layerDropdown, liAttribute, $this){
    var activeLayerFind = $($this).attr(activeDataAttr);
    $(layerDropdown + " li").removeClass('active');
    $(layerDropdown).find("[" + liAttribute + "=\"" + activeLayerFind + "\"]").addClass('active');
}
