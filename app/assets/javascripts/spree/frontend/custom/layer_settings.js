// Layer Panel Settings
var actualbgWidth = $('.main-img-layer').width();
var actualbgHeight = $('.main-img-layer').height();
$('.layer-settings input').on('input', function(){
    $this = $(this);
    var styleAttr = $(this).attr('data-style');
    var findLayerAttr = getLayerAttr($this)
    var findDocLayer = getScaleDocSection(findLayerAttr);
    var layerPreviewList = getLayerPreviewList(findLayerAttr);
    var valueSpan = $(this).prev('h3').find('.value-span-input');
    valueSpan.html($(this).val());
    switch(styleAttr) {
        case 'opacity':
        filterOpacity = ($(this).val() / 100).toFixed(1);
        findDocLayer.css('opacity', filterOpacity);
        layerPreviewList.attr('data-opacity', $(this).val());
        break;

        case 'rotate':
        findDocLayer.css('transform', "rotate(" + $(this).val() + "Deg)");
        layerPreviewList.attr('data-rotate', $(this).val());

        break;

        case 'lineheight':
        findDocLayer.css('line-height', $(this).val());
        layerPreviewList.attr('data-lineheight', $(this).val());

        break;

        case 'fontsize':
        findDocLayer.css('font-size', $(this).val() + "px");
        layerPreviewList.attr('data-fontsize', $(this).val());

        break;

        case 'fontfamily':
        findDocLayer.css('font-family', $(this).val().replace(/\+/g,' '));
        layerPreviewList.attr('data-value', $(this).val().replace(/\+/g,' '));
        break;

        case 'textalign':
        findDocLayer.css('text-align', $(this).val());
        layerPreviewList.attr('data-textalign', $(this).val());
        break;

        case 'fontweight':
        findDocLayer.css('font-weight', $(this).val());
        layerPreviewList.attr('data-fontweight', $(this).val());
        break;

        case 'fontitalic':
        findDocLayer.css('font-style', $(this).val());
        layerPreviewList.attr('data-fontitalic', $(this).val());
        break;

        case 'fontunderline':
        findDocLayer.css('text-decoration', $(this).val());
        layerPreviewList.attr('data-fontunderline', $(this).val());
        break;

        case 'fontcolor':
        findDocLayer.css('color', $(this).val());
        layerPreviewList.attr('data-fontcolor', $(this).val());
        break;

        case 'bgscale':
        $('.main-img-layer').css('width',  actualbgWidth * $(this).val() + 'px' );
        $('.main-img-layer').css('height',  actualbgHeight * $(this).val() + 'px' );
        break;

        case 'scale':
        findDocLayer.find('img').css('width', $(this).val() + 'px');
        layerPreviewList.attr('data-imgwidth', $(this).val());
        break;
    }
})

$('.layer-rotate-image').click(function(){
    $this = $(this);
    var findLayerAttr = getLayerAttr($this);
    var findDocLayer = getScaleDocSection(findLayerAttr);
    var selectedLayerImg = $(findDocLayer).find('.parent-mult-img img');
    var angle = ($(this).data('angle') + 90) || 90;
    $(selectedLayerImg).attr('angle', angle);

    if(angle == 360) {
        angle = 0; 
    }
    $this.data('angle', angle);
    $this.find('span').html(angle + ' Deg');

    setDimensionOfImage(selectedLayerImg); 
    rotateBase64Image(selectedLayerImg[0].src, angle, callback, selectedLayerImg);
});

rotateBase64Image = function(base64data, angle, callback, selectedLayerImg) {
    var canvas = document.getElementById("c");

    var naturalWidth, naturalHeight;
    naturalWidth = parseFloat(selectedLayerImg.attr('natural-width'));
    naturalHeight = parseFloat(selectedLayerImg.attr('natural-height'));

    if(angle == 90 || angle == 270) {
        canvas.width = naturalHeight;
        canvas.height = naturalWidth;
    } else {
        canvas.width = naturalWidth;
        canvas.height = naturalHeight;
    }

    var ctx = canvas.getContext("2d");
    ctx.imageSmoothingEnabled = false;

    var image = new Image();
    image.src = base64data;
    image.onload = function() {
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.save();
        ctx.translate(canvas.width/2,canvas.height/2);
        ctx.rotate(90*Math.PI/180);
        ctx.drawImage(image, -image.width/2,-image.height/2);
        ctx.restore();
        callback(canvas.toDataURL(), selectedLayerImg, angle);
    };
}

callback = function(base64data, selectedLayerImg, angle) {
    selectedLayerImg.attr('angle', angle);
    selectedLayerImg[0].src = base64data;
}
