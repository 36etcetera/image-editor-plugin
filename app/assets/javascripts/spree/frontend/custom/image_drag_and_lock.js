// ============== Background Drag & Center =============
$('.btn-drag').on('click', function(){
    var activeEffect = $(this).closest('.card-body').attr('data-effect');
    switch(activeEffect){
        case "background":
        imageLock(this, '.main-img-layer', activeEffect);
        break;
        case "cusImage":
        $this = $(this);
        var docActiveLayerAttr = getLayerAttr($this);
        var docActiveLayer = getScaleDocSection(docActiveLayerAttr);
        imageLock(this, docActiveLayer.find('img'), activeEffect);
        break;
    }
});
function imageLock($this, activeLayer, activeEffect){
    if ($($this).hasClass('btn-danger')) {
        $($this).removeClass('btn-danger');
        $($this).html('<i class="fas fa-unlock"></i>')
        $($this).addClass('btn-success');
        $(activeLayer).css('pointer-events', 'auto');
        activeEffect == 'background' ? $('#filter-main-bg').css('pointer-events', 'none') : null;

    } else {
        $($this).removeClass('btn-success');
        $($this).html('<i class="fas fa-lock"></i>')
        $($this).addClass('btn-danger');
        $(activeLayer).css('pointer-events', 'none');
        activeEffect == 'background' ? $('#filter-main-bg').css('pointe r-events', 'auto') : null;
    }
}

$('.btn-bg-center').on('click', function() {
    $('.main-img-layer').css('top', '0px');
    $('.main-img-layer').css('left', '0px');
});
