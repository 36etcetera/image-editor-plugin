// ============== Preview Image  =============
var beforeSaveWidth, beforeSaveHeight;
$('.save-btn').on('click', function(e){
    e.preventDefault();
    beforeSaveHeight = newDocument.height();
    beforeSaveWidth = newDocument.width();
    $('.loader-na').css('display', 'block');
    $('.layer-doc').removeClass('active-layer');
    newDocument.addClass('savingFinalImage');
    newDocument.css('width', scaleDocSection.width());
    newDocument.css('height', scaleDocSection.height());
    scaleDocSection.css('transform', 'none');
    var currentScaleDoc = $('.doc-scale').attr('data-current-scale');
    setTimeout(function(){htmlToCanvasImg(currentScaleDoc)}, 1000);
})

function htmlToCanvasImg(currentScaleDoc){
    html2canvas(document.querySelector(".scale-doc-section"), {
        scale: 1,
        allowTaint: true,
        backgroundColor: null
    }).then(function(canvas) {
        $('.final-img').html(canvas);
        newDocument.css('width', beforeSaveWidth);
        newDocument.css('height', beforeSaveHeight);
        newDocument.removeClass('savingFinalImage');
        scaleDocSection.css('transform', "scale(" + parseFloat(currentScaleDoc).toFixed(1) + ")");
        setTimeout(function(){$('.loader-na').css('display', 'none');}, 1000);
        $('#personalize_product_image_data').val(canvas.toDataURL());
    });
    EventToPassValueInProductPictures();
    EventToSetValueInProductBackgroundTableFields();
    EventToPassValueInProductTexts();
    EventToPassValueInProductStickers();
}
