var BackgroundPatterns = $('.background-patterns');
var touchtime = 0;
var getTime = new Date().getTime();
var mainImglayerImg = $('.main-img-layer img');
var layerDetailsPanel = $('.layer-details-panel');
var scaleDocSection = $('.scale-doc-section');
var productFrameId = $('#personalize_product_product_background_attributes_product_frame_id');
var productBackgroundImgData = $('#personalize_product_product_background_attributes_product_background_data');
var productBackgroundColor = $('#personalize_product_product_background_attributes_background_color');
var newDocument = $('.new-document');
var locale = $('#content-plugin').attr('locale');
locale = locale.length == 0 ? "en" : locale
var layerStick = $('.layer-stick');
var productFrame = $('#product-frame');
var predefinedText = $('.predefined-texts');
var CustomImageLayers = $('.card-body[data-effect="cusImage"]').find('.layer-dropdown-container li');
