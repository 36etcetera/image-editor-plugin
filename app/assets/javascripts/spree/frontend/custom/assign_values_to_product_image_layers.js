//================= Set value in Product Picture ====================//

function EventToPassValueInProductPictures(){
  $.each( CustomImageLayers, function(index, value) {
    var layerId = $(value).attr('data-layer-doc');
    var customImgWidth = $(value).attr('data-imgwidth');
    // var layerOpacity = $(value).attr('data-opacity');
    var ImageLayerSrc = $(value).find('.layer-right img').attr('src');
    var productWidth = $(value).attr('image-width');
    var productHeight = $(value).attr('image-height');

    var productPictureLayerObj = $('.layer-images-panel[id="'+layerId+'"]');

    var customWidthInputObj = productPictureLayerObj.find($('input[type=hidden][data-style="scale"]'));
    // var opacityInputObj = productPictureLayerObj.find($('input[type=hidden][data-style="opacity"]'));
    var layerImageInputObjWidth = productPictureLayerObj.find($('input[type=hidden][data-style="product_width"]'));
    var layerImageInputObjHeight = productPictureLayerObj.find($('input[type=hidden][data-style="product_height"]'));
    var layerImageInputObj = productPictureLayerObj.find($('input[type=file][data-style="product_image"]'));
    var imgWidth = (customImgWidth == undefined) ? productWidth : customImgWidth;
    // var imgOpacity = (layerOpacity == undefined ) ? '1' : layerOpacity;

    customWidthInputObj.val(imgWidth);
    // opacityInputObj.val(imgOpacity);
    layerImageInputObjWidth.val(productWidth);
    layerImageInputObjHeight.val(productHeight);

    bindEventToDeleteGalleryUploadedPicturesIfUploaded(layerImageInputObj, layerId);
  });
}

bindEventToDeleteGalleryUploadedPicturesIfUploaded = function(layerImageInput, targetLayerObj){
  layerImageInputObjValue = layerImageInput.val();
  if(layerImageInputObjValue.length > 0){
    var photoGalleryLayer = $('.photo-gallery[data-layer-doc="'+targetLayerObj+'"]');
    photoGalleryLayer.remove();
  }
}

//=================Set value in Product Background ====================//

function EventToSetValueInProductBackgroundTableFields(){
  setValueOfBgColorInProductBackgroundTableFields();
  setValueOfBgImgInProductBackgroundTableFields();
  setValueOfFrameInProductBackgroundTableFields();
}

setValueOfBgColorInProductBackgroundTableFields = function(){
  var targetEffect = productBackgroundColor.attr('data-effect');
  var color = scaleDocSection.attr('background-color');

  if(targetEffect == 'background_color'){
    if(color == undefined){
      productBackgroundColor.val('transparent'); 
    }
    else{
      productBackgroundColor.val(color);
    }
  }
  else{
    if(color == undefined){
      productBackgroundColor.val(targetEffect);
    }
    else {
      productBackgroundColor.val(color);
    }
  }
}

setValueOfBgImgInProductBackgroundTableFields = function(){
  var selectedPatternId = mainImglayerImg.attr('selected-pattern-id');

  if(selectedPatternId == undefined){
    var uploadedImageSrc = mainImglayerImg.attr('src');
    productBackgroundImgData.val(uploadedImageSrc);
  }
  else {
    productBackgroundImgData.val(parseInt(selectedPatternId));
  }
}

setValueOfFrameInProductBackgroundTableFields = function(){
  var uploadedFrameId = $('.frame-layer img').attr('frame');
  if(uploadedFrameId != undefined){
    productFrameId.val(uploadedFrameId);
  }
}
