// ============== Change image and check resolution ===============
$('.btn-change-pic input').on('change', function(){
    var activeAttrPicture = $(this).closest('label').attr('data-picture');
    switch(activeAttrPicture){
        case 'background':
          removeBackgroundPatternAttr();
          readURL(this, '.main-img-layer img');
        break;

        case 'layerpicture':
          var layerId = $(this).closest('.layer-images-panel').attr('id');
          uploadImage(this, layerId);
        break;
    }
});

uploadImage = function(dom, layerId){
  var selectedlayerDoc = getSelectedLayerDoc(layerId);
  var findLayerDoc = selectedlayerDoc.find('img')[0];
  var selectedlayerImgWidth = selectedlayerDoc.width();
  var selectedlayerImgHeight = selectedlayerDoc.height();
  var parentMultImg = selectedlayerDoc.find('.parent-mult-img');
  removeGalleryImageUploadedClass(parentMultImg);
  // setValueForResizingImage(selectedLayer);
  $('input[data-style="scale"]').css('border', 'none');
  var _URL = window.URL
  var file, img;
  if ((file = dom.files[0])) {
    img = new Image();
    img.onload = function() {
      if(this.width < selectedlayerImgWidth && this.height < selectedlayerImgHeight){
          $(dom).closest('.btn-change-pic').addClass('layer-error');
          alert(I18n[locale]["spree"]["js"]["images_resolution_error"]);
      }
      else{
        $(dom).closest('.btn-change-pic').removeClass('layer-error');
        readURL(dom, findLayerDoc, function(filelocation) {
          $(filelocation).attr('natural-width', 'undefined');
          $(filelocation).attr('natural-height', 'undefined');
          $(filelocation).attr('angle', '0');
        });
      }
    };
    img.src = _URL.createObjectURL(file);
  }
}

readURL = function (input, filelocation, callback) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $(filelocation).attr('src', e.target.result);
    }
    $(input).next('.filename').html(input.files[0].name);
    var activeLayerAttr = getLayerAttr(input);
    var activeDropdownLayer = $(input).closest('.layer-dropdown').find('.layer-dropdown-toggle').find("[data-layer-doc=\"" + activeLayerAttr + "\"]");
    activeDropdownLayer.attr('data-picture-name', input.files[0].name);
    reader.readAsDataURL(input.files[0]);
    setTimeout(function(){
      $(input).closest('.layer-details-panel').find('.layer-img').find('img').attr('src', $(filelocation).attr('src'));
      activeDropdownLayer.find('.layer-right').find('img').attr('src', $(filelocation).attr('src'));
    }, 1000);
    // $(input).val(null);
  }
  callback(filelocation);
}

removeBackgroundPatternAttr = function(){
  mainImglayerImg.removeAttr('selected-pattern-id');
}

$('.btn-change-pic').change(function(){
    $('.bg-image').css('display', 'block');
    $('.bg-image').css('opacity', '1');
});

$('input[data-style="scale"]').change(function(){
  var getlayerDetailsPanel  = $(this).closest(layerDetailsPanel);
  var layerDoc = getlayerDetailsPanel.attr('data-layer-doc');
  var selectedLayerImg = getlayerDetailsPanel.find('.layer-img img');
  var selectedLayerNaturalWidth = selectedLayerImg[0].naturalWidth;
  var selectedLayerNaturalHeight = selectedLayerImg[0].naturalHeight;
  var imageLayerDoc = getLayerForProductDimension(layerDoc);
  var parentMultImg = $(imageLayerDoc).find('.parent-mult-img img');
  var resizableWidth = $(parentMultImg).width();

  if(!$('.parent-mult-img').hasClass('added-through-gallery')){
    if(selectedLayerNaturalWidth < resizableWidth){
      $(this).css('border', '1px solid red');
      alert(I18n[locale]["spree"]["js"]["resize_resolution_error"]);
    }
    else{
      $(this).css('border', 'none');
    }
  }
});

removeGalleryImageUploadedClass = function(parentMultImg){    
  if (parentMultImg.hasClass('added-through-gallery')){
    parentMultImg.removeClass('added-through-gallery');
  }
}

BackgroundPatterns.click(function(){
  var src = $(this).attr('src');
  var selectedPatternId = $(this).attr('data-id');
  mainImglayerImg.attr('src', src);
  mainImglayerImg.css('display', 'block');
  mainImglayerImg.css('opacity', '1');
  mainImglayerImg.attr('selected-pattern-id', selectedPatternId);
  $(".remove_background_image").prop("checked", false);
});
