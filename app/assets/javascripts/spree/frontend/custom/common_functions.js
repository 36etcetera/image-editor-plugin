//= require spree/frontend/custom/select_layer_dropdown_and_its_properties.js

getScaleDocSection = function(LayerAttr){
  return scaleDocSection.find("[data-layer-doc=" + LayerAttr + "]");
}

getLayerAttr = function(dom){
  return $(dom).closest('.layer-details-panel').attr('data-layer-doc');
}

getLayerPreviewList = function(LayerAttr){
  return $('.layer-dropdown .dropdown-menu').find("[data-layer-doc=" + LayerAttr +"]");
}

getLayerForProductDimension = function(layerId){
  return $(".layer-image[data-layer-doc=" + layerId + "]");
}

productStickerLayerObjForLayerName = function(targetAttrVal) {
  return $(".product-stickers[data-layer-doc='" + targetAttrVal + "']");
};

productTextLayerObjForLayerName = function(targetAttrVal) {
  return $(".product-texts[data-layer-doc=" + targetAttrVal + "]");
};

getSelectedImagesLayerPanel = function(layerId){
  return $('.layer-images-panel[id="'+layerId+'"]');
}

getLayerDropDownContainer = function(activeAttribute){
  return $('.layer-dropdown-container').find("[data-layer-doc=" + activeAttribute + "]");
}

getSelectedLayerDoc = function(targetLayer){
  return $('.layer-doc[data-layer-doc="'+ targetLayer +'"]');
}

getlayerDetailsPanel = function(targetLayer){
   return $('.layer-details-panel[data-layer-doc="'+ targetLayer +'"]');
}

setValueForResizingImage = function(targetLayerAttr){
  var layerDropdownContainer = getLayerDropDownContainer(targetLayerAttr);
  var layerImgWidth = layerDropdownContainer.attr('data-image-width');
  var layerPanel = getlayerDetailsPanel(targetLayerAttr);
  layerSwitchDetails(layerImgWidth, '.layer-scale', 'data-imgwidth', this, layerPanel, 'input[data-style="scale"]');
}

//================bind data layers ==============================//
bindClassOnProductLayers = function(){
    var templateImageLength = $('.product-pictures').length;
    var templateTextLength = $('.product-texts').length;
    var templateStickerLength = $('.product-stickers').length;

    for (var i = 0; i < parseInt(templateImageLength); i++) {
        var productPictures = $('.product-pictures');
        $(productPictures[i]).attr('data-layer-doc', "layer-"+(i + 1));
    }

    for (var j = 0; j < parseInt(templateTextLength); j++) {
        var productTexts = $('.product-texts');
        $(productTexts[j]).attr('data-layer-doc', "layer-text-"+(j + 1));
    }

    for (var k = 0; k < parseInt(templateStickerLength); k++) {
        var productStickers = $('.product-stickers');
        $(productStickers[k]).attr('data-layer-doc', "layer-sticker-"+(k + 1));
    }
}

bindLayerOnProductGalleryLayers = function(){
  var galleryPhotoUploadedLength = $('.photo-gallery').length

  for (var l = 0; l < parseInt(galleryPhotoUploadedLength); l++){
    var galleryPhotos = $('.photo-gallery');
    $(galleryPhotos[l]).attr('data-layer-doc', "layer-"+(l + 1));
  }
}

bindFieldsForProductProperties = function(selectedLayer, parameterLength, targetProperty){
  var target = $(selectedLayer).data("target");
  var new_table_row = $(targetProperty).clone();
  var new_id = (parameterLength);
  new_table_row.find("input, select").each(function () {
    var el = $(this);    
    el.val("");
    el.prop("id", el.prop("id").replace(/\d+(?=[^\d]*$)/, new_id))
    el.prop("name", el.prop("name").replace(/\d+(?=[^\d]*$)/, new_id))
  })
  $(target).append(new_table_row);
}

setDimensionOfImage = function(selectedLayerImg){
   if(selectedLayerImg.attr('natural-width') == 'undefined') {
        selectedLayerImg.attr('natural-width', selectedLayerImg[0].naturalWidth);
        selectedLayerImg.attr('natural-height', selectedLayerImg[0].naturalHeight);
    }
}

colorpickerSpectrumBackground = function(){  
  $(".colorpicker").spectrum({
      flat: true,
      showInput: false,
      showInitial: true,
      allowEmpty: true, 
      showAlpha: false,
      hideAfterPaletteSelect:true,
      showPalette: false,
      showPaletteOnly: true,
      togglePaletteOnly: false,
      // togglePaletteMoreText: 'more',
      // togglePaletteLessText: 'less',
      color: null,
      default: false,
      change: function(color) {
        var input = $(this).parent().find('input#background-color.colorpicker');
        scaleDocSection.css('background-color', input.val());
        scaleDocSection.css('background-image', 'none');
        scaleDocSection.attr('background-color', input.val());
      },
      palette: [
        ["#0000ffff","#fff","#f3f3f3","#ccc","#999","#666","#444", "#000"],
        ["#faa", "#fea", "#dfa", "#afb", "#aff", "#abf", "#daf", "#fae"],
        ["#f55", "#fd5", "#af5", "#5f8", "#5ff", "#58f", "#a5f", "#f5d"],
        ["#f00", "#fb0", "#8f0", "#0f4", "#0ff", "#04f", "#80f", "#f0b"],
        ["#a00", "#a80", "#5a0", "#0a2", "#0aa", "#02a", "#50a", "#a08"],
        ["#500", "#540", "#250", "#051", "#055", "#015", "#205", "#504"],
        ["#fbb", "#feb", "#dfb", "#bfc", "#bff", "#bcf", "#dbf", "#fbe"],
        ["#e77", "#ec7", "#ae7", "#7e8", "#7ee", "#78e", "#a7e", "#e7c"],
        ["#d22", "#da2", "#8d2", "#2d5", "#2dd", "#25d", "#82d", "#d2a"],
        ["#811", "#871", "#581", "#183", "#188", "#138", "#518", "#817"],
        ["#400", "#430", "#240", "#041", "#044", "#014", "#204", "#403"],
        ["#ecc", "#edc", "#dec", "#cec", "#cee", "#cce", "#dce", "#ecd"],
        ["#c88", "#cb8", "#ac8", "#8c9", "#8cc", "#89c", "#a8c", "#c8b"],
        ["#a55", "#a95", "#8a5", "#5a6", "#5aa", "#56a", "#85a", "#a59"],
        ["#733", "#763", "#573", "#374", "#377", "#347", "#537", "#736"],
        ["#311", "#331", "#231", "#132", "#133", "#123", "#213", "#313"]
        // ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
        // ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
        // ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
        // ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
        // ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
        // ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
        // ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
      ]
  });
}


colorpickerSpectrum = function(){  
  $(".font-colorpicker").spectrum({
      showInput: false ,
      showInitial: true,
      allowEmpty: true, 
      showAlpha: true,
      hideAfterPaletteSelect:true,
      showPaletteOnly: true,
      togglePaletteOnly: true,
      togglePaletteMoreText: 'more',
      togglePaletteLessText: 'less',
      color: null,
      default: false,
      change: function(color) {
        var findLayerAttr = getLayerAttr($(this))
        var findDocLayer = getScaleDocSection(findLayerAttr);
        var layerPreviewList = getLayerPreviewList(findLayerAttr);
        findDocLayer.css('color', $(this).val());
        layerPreviewList.attr('data-fontcolor', $(this).val());
        $(this).closest('.text-styles').find('.color').css('background-color', $(this).val());
      },
      palette: [
        ["#0000ffff","#fff","#f3f3f3","#ccc","#999","#666","#444", "#000"],
        ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
        ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
        ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
        ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
        ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
        ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
        ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
      ]
  });
}

