// Font (Weight, Color, Italic, Underline)
$('.text-styles li').on('click', function(){
    var textStyleAttribute = $(this).attr('data-textstyle');

    switch(textStyleAttribute){
        case 'bold':
        inputTriggerTextStyles(this, '[data-style="fontweight"]', 'normal', textStyleAttribute);
        break;

        case 'italic':
        inputTriggerTextStyles(this, '[data-style="fontitalic"]', 'normal', textStyleAttribute);
        break;

        case 'underline':
        inputTriggerTextStyles(this, '[data-style="fontunderline"]', 'none', textStyleAttribute);
        break;
    }
})

// ============== Font Styles Value Change & Trigger OnInput(Bold, Italic, Underline) =============
inputTriggerTextStyles = function($this, inputDataAttr, defaultValue, textStyleAttribute){
    var inputTextStyle = $($this).closest('.layer-textproperties').find(inputDataAttr);
    if(inputTextStyle.val() == textStyleAttribute) {
        inputTextStyle.val(defaultValue);
        $($this).removeClass('active');
        $( ".layer-settings input" + inputDataAttr ).trigger( 'input' );
    } else {
        inputTextStyle.val(textStyleAttribute);
        $($this).addClass('active');
        $( ".layer-settings input" + inputDataAttr ).trigger( 'input' );
    }
}

inputTriggerTextProperties('.text-properties', 'data-aligntext', '.layer-textproperties', 'input[data-style="textalign"]');
inputTriggerTextProperties('.fs-results', 'data-value', '.layer-fontstyle', 'input[data-style="fontfamily"]');

// ============== Trigger OnInput Function For Dynamic Added Input Values Also (Active/Inactive) Classes =============
function inputTriggerTextProperties(textEffectDropdown, textEffectAttr, textEffectLayer, textEffectInput ){

    $(document).on('click', textEffectDropdown + " li", function(){
        var selectedFontEffect = $(this).attr(textEffectAttr);
        $(this).closest(textEffectLayer).find(textEffectInput).val(selectedFontEffect);
        $(textEffectDropdown + " li").removeClass('active');
        $(this).addClass('active');
        $( ".layer-settings " + textEffectInput ).trigger( 'input' );
    });
}

$('.scale-doc-section').delegate('.layer-text', 'click', function(){
    if(window.getSelection().type == 'Caret') {
        selectText($(this).find('.text-content').get(0));
    }
})
// ============ Select Text on click=====================
selectText = function(containerid) {
    var range = document.createRange();
    range.selectNodeContents(containerid);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
}

// On Selecting Outside of Text Layer Selected Text Will be unselected
 $('.scale-doc-section').delegate('.layer-text', 'blur', function(){
    window.getSelection().removeAllRanges();
 });
