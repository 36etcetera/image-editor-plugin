// Layer Effects Panel Open
$('.nav-item-hvr a').on('click', function(){
    if($(this).closest('.nav-item-hvr').hasClass('slide-active')) {
        $(this).closest('.nav-item-hvr').removeClass('slide-active');
        $(this).closest('.nav-item-hvr').find('.hover-items').slideUp();
        $('body').removeClass('hvr-active-layer');
    } else {
        $('.hover-items').slideUp();
        $('.nav-item-hvr').removeClass('slide-active');
        $(this).closest('.nav-item-hvr').addClass('slide-active');
        $(this).closest('.nav-item-hvr').find('.hover-items').slideDown();
        $('body').addClass('hvr-active-layer');
    }
});