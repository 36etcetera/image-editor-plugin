$('.frames-link .item-hvr').click(function(){
    var FrameUrl = $(this).attr('data-frame-url');
    var FrameID = $(this).attr('data-frame');
    productFrame.html('');
    productFrame.append("<div class=\"frame-layer\"><img src=\"" + FrameUrl + "\" frame=\"" + FrameID + "\"></div>");
    hvrItemsSlideUp();
});

// Slide Up Items Panel
function hvrItemsSlideUp(){
    $('.hover-items').slideUp();
    $('.nav-item-hvr').removeClass('slide-active');
    $('body').removeClass('hvr-active-layer');
}

// Stickers Layers Docs (Adding Layers to the Document)
var countLayer = 0;
$('.others-link .item-hvr').click(function(){
    countLayer = countLayer + 1;
    $('.layer-doc').removeClass('active-layer');
    switch($(this).attr('data-effect')){
        case 'stickers':
            var layerStick = $('.layer-stick');
            var layerStickerLength = layerStick.length;
            countLayer = layerStickerLength+1;
            var fileName = 'sticker';
            var layerName = "Sticker"
            var stickerId = $(this).attr('data-sticker-id');
            var stickerImg = $(this).attr('data-sticker-image-url');
            var docLayer = "<div class=\"draggable-layer layer-doc layer-stick active-layer\" data-layer-doc=\"layer-" + fileName + "-" + countLayer + "\" sticker-id=\"" + stickerId + "\"><span class=\"doc-layer-name\">" + layerName + "-" + countLayer + "</span><img src=\"" + stickerImg + "\"></div>";
            var aspectRatio = 1/1;
        break;

        case 'text':
            var layerName = "Text"
            productTextLayers = $('.product-texts').length
            countLayer = predefinedText.attr('data-template-texts') == "true" ? productTextLayers+1 : countLayer
            var fileName = $(this).attr('data-effect');
            var stickerImg = $("#layerDetailsPanel").data("imagePath");
            var docLayer = "<div class=\"draggable-layer layer-doc layer-text layer-text-drag active-layer\" data-layer-doc=\"layer-" + fileName + "-" + countLayer + "\" style=\"top: 125px; left: 125px; font-size: 76px; width: 600px;\"><span class=\"doc-layer-name\">" + layerName + "-" + countLayer + "</span><div class=\"text-content\" spellcheck=false contenteditable=\"true\">Lorem Ipsum is a simply dummy text</div><span class=\"editText\"><i class=\"fas fa-edit\"></i></span></div>";
            var aspectRatio = false;
        break;
    }
    scaleDocSection.append(docLayer);

    var layerController = "<li data-layer-doc=\"layer-" + fileName + "-" + countLayer + "\"><span class=\"layer-left\">" + layerName + "-" + countLayer + "</span><span class=\"layer-right\"><i class=\"fa fa-eye\"></i><img src=\"" + stickerImg + "\" /></span></li>";
    var activeEffect = $(this).attr('data-effect');
    var layerContainer = $(".layer-panel").find("[data-effect=\"" + activeEffect + "\"]");
    layerContainer.find('.layer-dropdown-container').append(layerController);
    $(".draggable-only").draggable({
        start: function (event, ui) {
            var left = parseInt($(this).css('left'),10);
            left = isNaN(left) ? 0 : left;
            var top = parseInt($(this).css('top'),10);
            top = isNaN(top) ? 0 : top;
            recoupLeft = left - ui.position.left;
            recoupTop = top - ui.position.top;
        },
        drag: function (event, ui) {
            ui.position.left += recoupLeft;
            ui.position.top += recoupTop;
        }
    })
    $( ".draggable-layer" )
        .draggable({
            start: function (event, ui) {
                var left = parseInt($(this).css('left'),10)
                left = isNaN(left) ? 0 : left;
                var top = parseInt($(this).css('top'),10);
                top = isNaN(top) ? 0 : top;
                recoupLeft = left - ui.position.left;
                recoupTop = top - ui.position.top;
            },
            drag: function (event, ui) {
                var mult = 20;
                var $dragme = $(event.target);
                ui.position.left += recoupLeft+mult;
                ui.position.top += recoupTop+mult;
                $dragme.css({
                    top: ui.position.top,
                    left: ui.position.left
                });
            }
        })
        .resizable({
            handles: 'se,e,w',
            aspectRatio: aspectRatio
        });
    hvrItemsSlideUp()
})
