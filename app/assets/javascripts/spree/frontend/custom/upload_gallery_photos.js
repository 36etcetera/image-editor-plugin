//================= Override spree_add_fields function =====================//
spreeAddGalleryPhotosBtn = function(){
  return $('.spree_add_gallery_photos');
}

bindClickEventOnAddGalleryPhotosBtn = function(){
  spreeAddGalleryPhotosBtn().off('click').on('click', function(event) {
    addPhotoGalleryFields($(this));
  });
}

addPhotoGalleryFields = function(targetBtn){
  var galleryForms = $('.photo-gallery').length;
  var galleryLastPhoto = $('.photo-gallery:last');
  bindFieldsForProductProperties(targetBtn, galleryForms, galleryLastPhoto);
}
// ================ Photo Gallery Select photo ======================

$(document).on('click', '.gallery', function(){
  setTimeout(function() {
    addActiveClass();
    loadAllGalleryPhotos();
  }, 50);
  getSelectedLayerForTheGalleryPhoto(this);
});

getSelectedLayerForTheGalleryPhoto = function(dom){
  var targetLayerAttr = $(dom).closest(layerDetailsPanel).attr('data-layer-doc')
  var findLayerDoc = getSelectedLayerDoc(targetLayerAttr).find('img')[0];
  var iconImg = $(dom).closest(layerDetailsPanel).find('.layer-img img');
  bindClickEventOnPhotoGalleryCard(findLayerDoc, iconImg, dom);
}

bindClickEventOnPhotoGalleryCard = function(location, iconImgLocation, target){
  var touchtime = 0;
  $(document).off('click', '.photo-gallery-card').on('click', '.photo-gallery-card', function(){
    if (touchtime == 0) {
        touchtime = new Date().getTime();
    } else {
        if (((new Date().getTime()) - touchtime) < 800) {
            var productId = $(this).attr('data-id');
            var variantId = $("input[name='variant_id']:checked").val();
            var targetLayer = $(target).closest('.layer-details-panel').attr('data-layer-doc');
            var selectedImg = $(".photo-gallery-card[data-id='" + productId + "']").find('img');
            var uploadedImgSrc = selectedImg.attr('src');
            var imgLocation = $(location).closest('.parent-mult-img');
            var Img = $(location).clone();

            $(target).closest('label').siblings('.system-file-upload').find('.value-span-input').html('');

            bindEventToAddSpreeNestedFields(targetLayer);
            uploadGallerySelectedPhoto(uploadedImgSrc, imgLocation, productId, Img, iconImgLocation );
            variantId = (variantId == undefined) ? '' : variantId;
            
            setValueForResizingImage(targetLayer);
            setValueInGalleryTable(targetLayer, productId, variantId);
            $("input[name='variant_id']:checked").val('');
            $('.close').trigger('click');
            touchtime = 0;
        } else {
            touchtime = new Date().getTime();
        }
    }  
  });
}

setValueInGalleryTable = function(targetLayer, productId, variantId){
    var productGalleryLayerObj = productGalleryLayerObjForLayerName(targetLayer);
    var InputUploadedProductIdObj = productGalleryLayerObj.find($('input[type=hidden][data-effect="uploaded_photo_id"]'));
    var InputQuantityObj = productGalleryLayerObj.find($('input[type=hidden][data-effect="quantity"]'));
    var InputVariantObj = productGalleryLayerObj.find($('input[type=hidden][data-effect="variant_id"]'));
    var InputLayer = productGalleryLayerObj.find($('input[type=hidden][data-effect="gallery_image_layer"]'));

    InputUploadedProductIdObj.val(productId);
    InputQuantityObj.val('1');
    InputVariantObj.val(variantId);
    InputLayer.val(targetLayer);

    bindEventToDeleteSystemUploadedPhoto(targetLayer);
}


uploadGallerySelectedPhoto = function(uploadedImgSrc, imgLocation, productId, Img, iconImgLocation){
    $(imgLocation).append("<div class=gallery-uploaded-image product_id=\"" + productId + "\"></div>");

    img_dom = $(imgLocation).find('img')
    $(imgLocation).addClass('added-through-gallery');
    img_dom.attr('src', uploadedImgSrc)
    $(iconImgLocation).attr('src', uploadedImgSrc);
}

bindEventToAddSpreeNestedFields = function(targetLayer){
  var galleryUploadImages = $('.gallery-uploaded-image');
  if(galleryUploadImages.length > 0){
    var uploadedGalleryImageOnLayer = $(galleryUploadImages).closest('[data-layer-doc="'+targetLayer+'"]');
    if (uploadedGalleryImageOnLayer.length != 1){
        addPhotoGalleryFields(spreeAddGalleryPhotosBtn());       
    }
  }
   $('.photo-gallery:last').attr('data-layer-doc', targetLayer);
}

productGalleryLayerObjForLayerName = function(targetAttrVal) {
    return $(".photo-gallery[data-layer-doc='" + targetAttrVal+ "']");
};

bindEventToDeleteSystemUploadedPhoto = function(targetLayer){
    $('.card-body[data-effect="cusImage"]').find()
    var ImageLayer = $('.card-body[data-effect="cusImage"]').find('.layer-dropdown-container li[data-layer-doc="'+targetLayer+'"]');
    ImageLayer.attr('data-picture-name', '');
    ImageLayer.find('.layer-right img').attr('src', '');
}

// ================Add Photo Gallery product in cart=================

saveSelectedGalleryPhoto = function(){
    var selectedGalleryPhoto = $('.selected-gallery-photo');
    if (selectedGalleryPhoto.length > 0){
        var targetLayer = 'layer-1';
        var productId = $(selectedGalleryPhoto).attr('product_id');
        var variantId = $(selectedGalleryPhoto).attr('variant_id');
        setValueInGalleryTable(targetLayer, productId, variantId);
    }
}

addActiveClass = function(){
  $(document).on('click', '.filter-taxon-btn', function(){
      if($(this).hasClass('btn-active')){
        $(this).removeClass('btn-active');
      }
      else {
        $('.all-photos-btn').removeClass('btn-active');
        $(this).addClass('btn-active');
        $('.all-photos-btn input').prop('checked', false);
      }
      var arr = []
      $('.search-gallery').find('.filter-taxon-btn.btn-active').each(function(){
          arr.push($(this).data('child'));
      });
      if(arr.length == 0){
        $('.all-photos input').prop('checked', true);
      }
      $.ajax({
        type: "GET",
        url: "/photo_galleries/photo-gallery",
        dataType: 'script',
        data: {
          selected_taxon: arr
        }
      });
  });
}

loadAllGalleryPhotos = function(){
  $(document).on('click', '.all-photos-btn', function(){
    $(this).addClass('btn-active');
    $('.search-gallery').find('.filter-taxon-btn.btn-active').each(function(){
      $(this).trigger('click');
    });
  });
}
