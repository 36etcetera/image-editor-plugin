//==================Set value in product stickers layer====================//
$('.item-hvr[data-effect="stickers"]').click(function(){
    $('.spree_add_stickers_fields').trigger('click');
    $('.product-stickers').removeAttr('data-layer-doc');
    bindClassOnProductLayers();
});

//================= Override spree_add_fields function =====================//
$('.spree_add_stickers_fields').click(function() {
  var templateStickersLength = $('.product-stickers').length;
  var productLastSticker = $('.product-stickers:last');
  bindFieldsForProductProperties($(this), templateStickersLength, productLastSticker);
});

EventToPassValueInProductStickers = function(){
    var CustomStickerLayers = $('.card-body[data-effect="stickers"]').find('.layer-dropdown-container li');
    $.each( CustomStickerLayers, function(index, value) {
        var stickerLayerId = $(value).attr('data-layer-doc');
        var stickerLayerRotate = $(value).attr('data-rotate');
        var stickerLayerOpacity = $(value).attr('data-opacity');
        var draggablelayer = $(".layer-stick[data-layer-doc='" + stickerLayerId + "']");

        var stickerLayerPostionX = draggablelayer.attr('style');
        stickerLayerPostionX = stickerLayerPostionX == undefined ? '0' :stickerLayerPostionX.split(";")[0].replace(/[^0-9]/gi, '');

        var stickerLayerPostionY = draggablelayer.attr('style');
        stickerLayerPostionY = stickerLayerPostionY == undefined ? '0' : stickerLayerPostionY.split(";")[1].replace(/[^0-9]/gi, '');

        var stickerLayerImageId = draggablelayer.attr('sticker-id');
        var productStickerLayerObj = productStickerLayerObjForLayerName(stickerLayerId);

        var InputIdObj = productStickerLayerObj.find($('input[type=hidden][data-effect="sticker_layer"]'));
        var InputObjRotate = productStickerLayerObj.find($('input[type=hidden][data-effect="rotate"]'));
        var InputObjOpacity = productStickerLayerObj.find($('input[type=hidden][data-effect="opacity"]'));
        var InputObjPositionX = productStickerLayerObj.find($('input[type=hidden][data-effect="position_x"]'));
        var InputObjPositionY = productStickerLayerObj.find($('input[type=hidden][data-effect="position_y"]'));
        var InputObjStickerId = productStickerLayerObj.find($('input[type=hidden][data-effect="sticker_id"]'));

        InputIdObj.val(stickerLayerId);
        InputObjRotate.val(stickerLayerRotate);
        InputObjOpacity.val(stickerLayerOpacity);
        InputObjPositionX.val(stickerLayerPostionX);
        InputObjPositionY.val(stickerLayerPostionY);    
        InputObjStickerId.val(stickerLayerImageId);
    }); 
}
