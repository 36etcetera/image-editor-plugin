// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/frontend/all.js'

$(document).ready(function(){
  var inputVariant = $('input[type=radio][name=variant_id]');
  var personalizeBtnObj = $('#open-personalize-btn');
  var targetPath = personalizeBtnObj.attr('href');
  var defaultInputVariantValue = inputVariant.val();
  var photogalleryCard = $('.photo-gallery-card');
  var selectGalleryPhoto = $('.select-gallery-photo');

  if(inputVariant.length > 0){
    if($('[data-hook="product_show"]').length == 1){
      getVariantInPath(defaultInputVariantValue);
    }

    inputVariant.on('change', function(){
      var selectedVariantId = $(this).val();
      getVariantInPath(selectedVariantId);
    });
  }

  function getVariantInPath(selectedVariantId){
    var extraParams = "?variant_id=" + selectedVariantId;
    var fullPath = targetPath  + extraParams;
    personalizeBtnObj.attr('href', fullPath);
  }

  $(photogalleryCard).on('mouseenter', function(){
    $(selectGalleryPhoto).removeClass('d-none');
  });

  $(photogalleryCard).on('mouseleave', function(){
    $(selectGalleryPhoto).addClass('d-none');
  });

  $(document).on('click', '.select-gallery-photo', function(){
    var variantId = $("input[name='variant_id']:checked").val();
    href = $(this).attr('href');
    if (variantId.length > 0){
      $(this).attr('href', href + "?variant=" + variantId);
    }
  });

  $('#detect_browser').on('click', function(){
    var locale = $('#open-personalize-btn').attr('selected_locale');
    locale.length == 0 ? "en" : locale
    browsers = {
      Chrome: 45,
      Firefox: 38,
      Opera: 30,
      Edge: 12,
      iOS: 9,
      Safari: 9,
    }

    browser = navigator.browserSpecs;

    if(browser.name == 'IE' || browser.name == 'msie' || browser.name  == "MSIE"  ){

      alert(I18n[locale]["spree"]["js"]["browser_error"]["not_supported_version"]);
    } else if(parseInt(browser.version) < browsers[browser.name]){
      alert(I18n[locale]["spree"]["js"]["browser_error"]["current_version_problem"]);
    } else {
      window.location = personalizeBtnObj.attr('href');
    }
  });

  navigator.browserSpecs = (function(){
    var ua = navigator.userAgent, tem, 
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return {name:'IE',version:(tem[1] || '')};
    }
    if(M[1]=== 'Chrome'){
      tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
      if(tem != null) return {name:tem[1].replace('OPR', 'Opera'),version:tem[2]};
    }
    M = M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem = ua.match(/version\/(\d+)/i))!= null)
      M.splice(1, 1, tem[1]);
    return {name:M[0], version:M[1]};
  })();
});
