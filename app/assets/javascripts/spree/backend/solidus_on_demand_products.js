// Placeholder manifest file.
// the installer will append this file to the app vendored assets here: vendor/assets/javascripts/spree/backend/all.js'

//= require_tree ./js
//= require spree/backend/js/bootstrap-colorpicker.min.js
//= require spree/backend/js/jquery.fontselect.js
//= require spree/backend/js/custom.js
//= require spree/admin/filters.js
//= require spree/admin/templates.js