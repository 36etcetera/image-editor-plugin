$(document).ready(function(){
  $('#product_wishlist_photos').change(function(){
    if (this.checked){
      $.ajax({
        url: 'get_taxons',
        type: 'POST',
        data: {"product_taxon_ids": $('#product_taxon_ids').val()}
      });
    }
    else{
      $('.wishlist-error').html('');
    }
  });
});
