class ResizeProductImageJob < ApplicationJob
  queue_as :default
  
  def perform(image_id)
    image = Spree::Image.find image_id
    template = image.viewable.product.template
    resolution = template.resolution.zero? ? 300 : template.resolution
    mini_magick = MiniMagick::Image.new(image.attachment.path(:original))
    mini_magick.density "#{resolution}"
  end
end
