$(document).ready(function(){
    $('.scale-doc-section').css('transform', 'scale(1)');
})

// Sidebar
$('.btn-toggle-sb').click(function(){
    $('.sidebar').toggleClass('sidebar-active');
    $('.main-layout').toggleClass('sb-active');
})

// Instructions
$('#disable-instructions').click(function(){
    $(".instruction-new").remove();
});

// Start Screen Window
$('.btn-toggle').click(function(){
    $('.select-links').addClass('active');
    $(this).closest('.select-links').removeClass('active');
})

// Zoom in & out
$('.doc-scale .btn').click(function(){
    var attrNature = $(this).attr('data-zoom');
    var currentScale = parseFloat($(this).closest('.doc-scale').attr('data-current-scale'));
    var newValue;
    
    switch(attrNature){
        case "positive":
        valueChange(+0.2, this, currentScale, attrNature, newValue);
        break;

        case "negative":
        valueChange(-0.2, this, currentScale, attrNature, newValue);
        break;
    }
})

// Dragable & Resizable


$('.draggableonly')
    .draggable({
        start: function (event, ui) {
            var left = parseInt($(this).css('left'),10);
            left = isNaN(left) ? 0 : left;
            var top = parseInt($(this).css('top'),10);
            top = isNaN(top) ? 0 : top;
            recoupLeft = left - ui.position.left;
            recoupTop = top - ui.position.top;
        },
        drag: function (event, ui) {
            ui.position.left += recoupLeft;
            ui.position.top += recoupTop;
        }
    });

// Layer Effects Panel Open
$('.nav-item-hvr').on('click', function(){
    if($(this).hasClass('slide-active')) {
        $(this).removeClass('slide-active');
        $(this).find('.hover-items').slideUp();
        $('body').removeClass('hvr-active-layer');
    } else {
        $('.hover-items').slideUp();
        $('.nav-item-hvr').removeClass('slide-active');
        $(this).addClass('slide-active');
        $(this).find('.hover-items').slideDown();
        $('body').addClass('hvr-active-layer');
    }
});

// filters Section
$('.filters-link .item-hvr').click(function(){
    var classFilter = $(this).attr('data-filter');
    var filterLocation = $('#filter-main-bg');
    var container = $(".layer-panel").find(`[data-effect="filters"]`);

    if(filterLocation.hasClass(classFilter)) {
        filterLocation.removeClass(classFilter);
        container.html('');
    } else {
        container.html('');
        filterLocation.removeClass();
        filterLocation.addClass(classFilter);
        container.append(
            '<div class="layer layer-filter"><h3>Filter Opacity</h3><input type="range" id="filter-opacity" min="0" max="100" value="100"></div>'
        )
        opacityModeFilter();
    }
})

// Layers Docs (Adding Layers to the Document)
var countLayer = 0;
$('.others-link .item-hvr').click(function(){
    
    countLayer = countLayer + 1;
    var documentMain = $('.scale-doc-section');
    $('.layer-doc').removeClass('active-layer');


    switch($(this).attr('data-effect')){
        case 'stickers': 
            var fileName = $(this).attr('data-sticker');
            var stickerImg = `img/${fileName}.png`;
            var docLayer = `<div class="draggable-layer layer-doc layer-stick active-layer" data-layer-doc="${fileName}-${countLayer}"><span class="doc-layer-name">Layer-${countLayer}</span><img src="${stickerImg}"></div>`;
            var aspectRatio = 1/1;
        break;

        case 'text':
            var fileName = $(this).attr('data-effect');
            var stickerImg = `img/${fileName}.png`;
            var docLayer = `<div class="draggable-layer layer-doc layer-text active-layer" data-layer-doc="${fileName}-${countLayer}"><span class="doc-layer-name">Layer-${countLayer}</span><div class="text-content" spellcheck=false contenteditable="true">Lorem Ipsum is a simply dummy text</div><span class="editText"><i class="fas fa-edit"></i></span></div>`;
            var aspectRatio = false;
        break;

        case 'cusImage':
            var fileName = $(this).attr('data-effect');
            var stickerImg = `img/${fileName}.png`;
            var docLayer = `<div class="draggable-layer layer-doc layer-image active-layer" data-layer-doc="${fileName}-${countLayer}"><span class="doc-layer-name">Layer-${countLayer}</span><div class="parent-mult-img"><img style="pointer-events: none" class="draggable-only" src="img/dummyImage.jpg"></div></div>`;
            var aspectRatio = false;
        break;
    }
    documentMain.append(docLayer);

    var layerController = `<li data-layer-doc="${fileName}-${countLayer}"><span class="layer-left">Layer-${countLayer}</span><span class="layer-right"><i class="fa fa-eye"></i><img src="${stickerImg}" /></span></li>`;
    var activeEffect = $(this).attr('data-effect');
    var layerContainer = $(".layer-panel").find(`[data-effect="${activeEffect}"]`);
    layerContainer.find('.layer-dropdown-container').append(layerController);
    $(".draggable-only").draggable({
        start: function (event, ui) {
            var left = parseInt($(this).css('left'),10);
            left = isNaN(left) ? 0 : left;
            var top = parseInt($(this).css('top'),10);
            top = isNaN(top) ? 0 : top;
            recoupLeft = left - ui.position.left;
            recoupTop = top - ui.position.top;
        },
        drag: function (event, ui) {
            ui.position.left += recoupLeft;
            ui.position.top += recoupTop;
        }
    })
    $( ".draggable-layer" )
        .draggable({
            start: function (event, ui) {
                var left = parseInt($(this).css('left'),10);
                left = isNaN(left) ? 0 : left;
                var top = parseInt($(this).css('top'),10);
                top = isNaN(top) ? 0 : top;
                recoupLeft = left - ui.position.left;
                recoupTop = top - ui.position.top;
            },
            drag: function (event, ui) {
                ui.position.left += recoupLeft;
                ui.position.top += recoupTop;
            }
        })
        .resizable({
            handles: 'se,e,w',
            aspectRatio: aspectRatio
        });
})

// On Selecting text Layer Dragging Disabled

 $('.scale-doc-section').delegate('.layer-text', 'click', function(){
    $(this).draggable({
        disabled: true
    });
    $(this).find('.text-content').focus();
 });

 // On Selecting Outside of Text Layer Selected Text Will be unselected

 $('.scale-doc-section').delegate('.layer-text', 'blur', function(){
    window.getSelection().removeAllRanges();
 });


$('.layer-dropdown').delegate('li .fa', 'click', function(){
    if($(this).hasClass('fa-eye')) {
        $(this).removeClass('fa-eye')
        $(this).addClass('fa-eye-slash');
    } else {
        $(this).addClass('fa-eye');
        $(this).removeClass('fa-eye-slash');
    }
    
    var findLayer = $(this).closest('li').attr('data-layer-doc');
    var findDocLayer = $('.scale-doc-section').find(`[data-layer-doc="${findLayer}"]`);
    findDocLayer.toggleClass('layer-hidden');
})


// Layer Active

$('#filter-main-bg').on('click', function(){
    activeLayerDisabled();
});

$('.main-img-layer').on('click', function(){
    activeLayerDisabled();
});

$('.scale-doc-section').delegate('.layer-doc', 'click', function(){
    $('.layer-doc').removeClass('active-layer');
    $(this).addClass('active-layer');
    $('.sidebar-toggle').addClass('active');
})


// Layer Select Dropdown

$('.layer-dropdown .layer-dropdown-toggle').delegate('li', 'click', function(){
    $('.layer-dropdown .layer-dropdown-toggle li').removeClass('active');
    $(this).addClass('active');
    var layerPanel = $(this).closest('.layer-dropdown').find('.layer-details-panel');
    var attrFind = $(this).attr('data-layer-doc');
    var layerName = $(this).find('.layer-left').html();
    var layerImage = $(this).find('.layer-right img').attr('src');
    var layerDocFind = $('.scale-doc-section').find(`[data-layer-doc="${attrFind}"]`);

    // Active & Non Active Switch Classes In Document
    $('.layer-doc').removeClass('active-layer');
    layerDocFind.addClass('active-layer');
    // --End--

    layerPanel.removeClass('disabled');
    layerPanel.attr('data-layer-doc', attrFind);
    layerPanel.find('.layer-name').html(layerName);
    layerPanel.find('.layer-img').html(`<img src="${layerImage}" />`);

    // Adding Active Layer Name to the Dropdown Button
    $(this).closest('.layer-dropdown-toggle').find('.dropdown-toggle').html(layerName);


    // Layer Input Value Switch
    layerSwitchDetails('0', '.layer-rotate', 'data-rotate', this, layerPanel, 'input');
    layerSwitchDetails('100', '.layer-opacity', 'data-opacity', this, layerPanel, 'input');
    layerSwitchDetails('14', '.layer-fontsize', 'data-fontsize', this, layerPanel, 'input');
    layerSwitchDetails('14', '.layer-lineheight', 'data-lineheight', this, layerPanel, 'input');
    layerSwitchDetails('left', '.layer-textproperties', 'data-textalign', this, layerPanel, 'input[data-style="textalign"]');
    layerSwitchDetails('', '.layer-fontstyle', 'data-fontfamily', this, layerPanel, 'input[data-style="fontfamily"]');

    layerSwitchDetails('normal', '.layer-textproperties', 'data-fontitalic', this, layerPanel, 'input[data-style="fontitalic"]');
    layerSwitchDetails('normal', '.layer-textproperties', 'data-fontweight', this, layerPanel, 'input[data-style="fontweight"]');
    layerSwitchDetails('none', '.layer-textproperties', 'data-fontunderline', this, layerPanel, 'input[data-style="fontunderline"]');
    layerSwitchDetails('#333', '.layer-textproperties', 'data-fontcolor', this, layerPanel, 'input[data-style="fontcolor"]');

    layerSwitchDetails('200', '.layer-scale', 'data-imgwidth', this, layerPanel, 'input[data-style="scale"]');
    layerSwitchDetails('Default', '.layer-pic', 'data-picture-name', this, layerPanel, 'input');

    if($(this).attr('data-brradius') == 'oval') {
        layerPanel.find('.layer-brradius').find('.btn-oval').addClass('oval-active btn-success');
        layerPanel.find('.layer-brradius').find('input').attr('disabled', 'disabled');
        
    } else {
        layerSwitchDetails('0', '.layer-brradius', 'data-brradius', this, layerPanel, 'input');
        layerPanel.find('.layer-brradius').find('input').removeAttr('disabled');
        layerPanel.find('.layer-brradius').find('.btn-oval').removeClass('oval-active btn-success');
    }

    // Font Family Dropdown Active/Non Active
    activeDropdownTextProperties('data-fontfamily', '.font-dropdown', 'data-fontstyle', this);
    activeDropdownTextProperties('data-textalign', '.text-properties', 'data-aligntext', this);

    activeDropdownFontStyle('data-fontitalic', '.text-styles', 'data-textstyle', this, 'italic');
    activeDropdownFontStyle('data-fontweight', '.text-styles', 'data-textstyle', this, 'bold');
    activeDropdownFontStyle('data-fontunderline', '.text-styles', 'data-textstyle', this, 'underline');

    colorSwitch('data-fontcolor', '.text-styles', 'data-textstyle', this);
})

// Layer Panel Settings
var actualbgWidth = $('.main-img-layer').width();
var actualbgHeight = $('.main-img-layer').height();
$('.layer-settings input').on('input', function(){
    var styleAttr = $(this).attr('data-style');
    var findLayerAttr = $(this).closest('.layer-details-panel').attr('data-layer-doc');
    var findDocLayer = $('.scale-doc-section').find(`[data-layer-doc="${findLayerAttr}"]`);
    var layerPreviewList = $('.layer-dropdown .dropdown-menu').find(`[data-layer-doc="${findLayerAttr}"]`);
    var valueSpan = $(this).prev('h3').find('.value-span-input');
    valueSpan.html($(this).val());

    switch(styleAttr) {
        case 'opacity':
        filterOpacity = ($(this).val() / 100).toFixed(1);
        findDocLayer.css('opacity', filterOpacity);
        layerPreviewList.attr('data-opacity', $(this).val());
        break;

        case 'rotate':
        findDocLayer.css('transform', `rotate(${$(this).val()}Deg)`);
        layerPreviewList.attr('data-rotate', $(this).val());
        
        break;

        case 'lineheight':
        findDocLayer.css('line-height', `${$(this).val()}px`);
        layerPreviewList.attr('data-lineheight', $(this).val());
        
        break;

        case 'fontsize':
        findDocLayer.css('font-size', `${$(this).val()}px`);
        layerPreviewList.attr('data-fontsize', $(this).val());
        
        break;

        case 'fontfamily':
        findDocLayer.css('font-family', $(this).val());
        layerPreviewList.attr('data-fontfamily', $(this).val());
        break;

        case 'textalign':
        findDocLayer.css('text-align', $(this).val());
        layerPreviewList.attr('data-textalign', $(this).val());
        break;

        case 'fontweight':
        findDocLayer.css('font-weight', $(this).val());
        layerPreviewList.attr('data-fontweight', $(this).val());
        break;

        case 'fontitalic':
        findDocLayer.css('font-style', $(this).val());
        layerPreviewList.attr('data-fontitalic', $(this).val());
        break;

        case 'fontunderline':
        findDocLayer.css('text-decoration', $(this).val());
        layerPreviewList.attr('data-fontunderline', $(this).val());
        break;

        case 'fontcolor':
        findDocLayer.css('color', $(this).val());
        layerPreviewList.attr('data-fontcolor', $(this).val());
        break;

        case 'bgscale':
        $('.main-img-layer').css('width',  actualbgWidth * $(this).val() + 'px' );
        $('.main-img-layer').css('height',  actualbgHeight * $(this).val() + 'px' );
        break;

        case 'scale':
        findDocLayer.find('img').css('width', $(this).val() + 'px');
        layerPreviewList.attr('data-imgwidth', $(this).val());
        break;

        case 'brradius':
        findDocLayer.find('.parent-mult-img').css('border-radius', $(this).val() + 'px');
        layerPreviewList.attr('data-brradius', $(this).val());
        break;
    }
})


// Font Styling
 
inputTriggerTextProperties('.text-properties', 'data-aligntext', '.layer-textproperties', 'input[data-style="textalign"]');
inputTriggerTextProperties('.font-dropdown', 'data-fontstyle', '.layer-fontstyle', 'input[data-style="fontfamily"]');


// Font (Weight, Color, Italic, Underline)
$('.text-styles li').on('click', function(){
    var textStyleAttribute = $(this).attr('data-textstyle');
    
    switch(textStyleAttribute){
        case 'bold':
        inputTriggerTextStyles(this, '[data-style="fontweight"]', 'normal', textStyleAttribute);
        break;

        case 'italic':
        inputTriggerTextStyles(this, '[data-style="fontitalic"]', 'normal', textStyleAttribute);
        break;

        case 'underline':
        inputTriggerTextStyles(this, '[data-style="fontunderline"]', 'none', textStyleAttribute);
        break;
    }
})

// ============== Circle & Oval Convert =============

$('.layer-brradius .btn-oval').on('click', function(){
    var activeLayerAttr = $(this).closest('.layer-details-panel').attr('data-layer-doc');
    var dropActiveLayer = $(this).closest('.layer-dropdown').find('.layer-dropdown-toggle').find(`[data-layer-doc="${activeLayerAttr}"]`);
    var mainDocument = $('.scale-doc-section').find(`[data-layer-doc="${activeLayerAttr}"]`).find('.parent-mult-img');
    $(this).prev('input').val('0');
    $(this).closest('.layer-brradius').find('.value-span-input').html('0');
    if($(this).hasClass('oval-active')) {
        $(this).prev('input').removeAttr('disabled');
        dropActiveLayer.attr('data-brradius', '0');
        $(this).removeClass('oval-active btn-success');
        mainDocument.css('border-radius', '0px');
    } else {
        $(this).addClass('oval-active btn-success');
        $(this).prev('input').attr('disabled', 'disabled');
        dropActiveLayer.attr('data-brradius', 'oval');
        mainDocument.css('border-radius', '100%');
    }

});

// All Functions

// ============== Zoom-In & Zoom-Out Document Value Change =============

function valueChange(valueOperator, $this, currentScale, attrNature, newValue){
    if(currentScale < 0.3 && attrNature == "negative" || currentScale > 1.5 && attrNature == "positive") {
        newValue = currentScale;    
    } else {
        newValue = currentScale + valueOperator;
    }
    if(newValue == 1) {
        $('.current-size').html('Scale: Original')
    } else {
        $('.current-size').html(`Scale: ${newValue.toFixed(2)}`)
    }
    $('.scale-doc-section').css('transform', `scale(${newValue})`).promise().done(function(){
        setTimeout(function(){
            var afterScaleHeight = $('.scale-doc-section').get(0).getBoundingClientRect().height;
            var afterScaleWidth = $('.scale-doc-section').get(0).getBoundingClientRect().width;
            $('.new-document').css('width', `${afterScaleWidth}px`);
            $('.new-document').css('height', `${afterScaleHeight}px`);
        }, 400)
    });

    $($this).closest('.doc-scale').attr('data-current-scale', `${newValue}`); 
}

// ============== Filters Opacity Change =============

function opacityModeFilter(){
    $('#filter-opacity').on('input', function(){
        filterOpacity = ($(this).val() / 100).toFixed(1);
        var filterLocation = $('#filter-main-bg');
        filterLocation.css('opacity', filterOpacity);
    })
}

// ============== Text Properties(FontStyle & TextAlign) Change =============


function activeDropdownTextProperties(activeDataAttr, layerDropdown, liAttribute, $this){
    var activeLayerFind = $($this).attr(activeDataAttr);
    $(`${layerDropdown} li`).removeClass('active');
    $(layerDropdown).find(`[${liAttribute}="${activeLayerFind}"]`).addClass('active');
}

function activeDropdownFontStyle(activeDataAttr, layerDropdown, liAttribute, $this, laterAttributeName){
    var activeLayerFind = $($this).attr(activeDataAttr);
    if(typeof activeLayerFind !== 'undefined' && activeLayerFind == laterAttributeName) {
        $(layerDropdown).find(`[${liAttribute}="${activeLayerFind}"]`).addClass('active');
    } else {
        $(layerDropdown).find(`[${liAttribute}="${laterAttributeName}"]`).removeClass('active');
    }
}
function colorSwitch(activeDataAttr, layerDropdown, liAttribute, $this){
    var activeColor = $($this).attr(activeDataAttr);
    var activeSpanWithColor = $(layerDropdown).find(`[${liAttribute}="color"]`).find('span');
    if(typeof activeColor === 'undefined') {
        activeSpanWithColor.css('background-color', '#333');    
    } else {
        activeSpanWithColor.css('background-color', activeColor);
    }
}

// ============== Layer Switch Value Change In Layer Settings =============


function layerSwitchDetails(defaultInputValue, layerClass, laterAttribute, $this, layerPanelParam, inputSelector) {
    if(typeof $($this).attr(laterAttribute) !== 'undefined'){
        laterAttribute !== 'data-picture-name' ? layerPanelParam.find(layerClass).find(inputSelector).val($($this).attr(laterAttribute)) : null;
        layerPanelParam.find(layerClass).find('.value-span-input').html($($this).attr(laterAttribute));
    } else {
        laterAttribute !== 'data-picture-name' ? layerPanelParam.find(layerClass).find(inputSelector).val(defaultInputValue) : null;
        layerPanelParam.find(layerClass).find('.value-span-input').html(defaultInputValue);
    }        
}

// ============== Trigger OnInput Function For Dynamic Added Input Values Also (Active/Inactive) Classes =============

function inputTriggerTextProperties(textEffectDropdown, textEffectAttr, textEffectLayer, textEffectInput ){
    $(`${textEffectDropdown} li`).on('click', function(){
        var selectedFontEffect = $(this).attr(textEffectAttr);
        $(this).closest(textEffectLayer).find(textEffectInput).val(selectedFontEffect);
        $(`${textEffectDropdown} li`).removeClass('active');
        $(this).addClass('active');
        $( `.layer-settings ${textEffectInput}` ).trigger( 'input' );
    });
}

// ============== Font Styles Value Change & Trigger OnInput(Bold, Italic, Underline) =============

function inputTriggerTextStyles($this, inputDataAttr, defaultValue, textStyleAttribute){
    var inputTextStyle = $($this).closest('.layer-textproperties').find(inputDataAttr);
    if(inputTextStyle.val() == textStyleAttribute) {
        inputTextStyle.val(defaultValue);
        $($this).removeClass('active');
        $( `.layer-settings input${inputDataAttr}` ).trigger( 'input' );
    } else {
        inputTextStyle.val(textStyleAttribute);
        $($this).addClass('active');
        $( `.layer-settings input${inputDataAttr}` ).trigger( 'input' );
    }
}

// ============== Background Drag & Center =============
$('.btn-drag').on('click', function(){
    var activeEffect = $(this).closest('.card-body').attr('data-effect');
    switch(activeEffect){
        case "background":
        imageLock(this, '.main-img-layer', activeEffect);
        break;
        case "cusImage":
        var docActiveLayerAttr = $(this).closest('.layer-details-panel').attr('data-layer-doc');
        var docActiveLayer = $('.scale-doc-section').find(`[data-layer-doc="${docActiveLayerAttr}"]`);
        imageLock(this, docActiveLayer.find('img'), activeEffect);
        break;
    }
});
function imageLock($this, activeLayer, activeEffect){
    if ($($this).hasClass('btn-danger')) {
        $($this).removeClass('btn-danger');
        $($this).addClass('btn-success');
        $($this).find('span').html('Unlocked');
        $(activeLayer).css('pointer-events', 'auto');
        activeEffect == 'background' ? $('#filter-main-bg').css('pointer-events', 'none') : null;
        
    } else {
        $($this).removeClass('btn-success');
        $($this).addClass('btn-danger');
        $($this).find('span').html('Locked');
        $(activeLayer).css('pointer-events', 'none');
        activeEffect == 'background' ? $('#filter-main-bg').css('pointer-events', 'auto') : null;
    }
}


$('.btn-bg-center').on('click', function() {
    $('.main-img-layer').css('top', '0px');
    $('.main-img-layer').css('left', '0px');
});

// ============== Background Hide & Show =============

$('.hide-bg-img .btn').on('click', function(){
    if($(this).hasClass('bg-hide')) {
        $(this).removeClass('bg-hide btn-danger');
        $(this).find('span').html('Hide');
        $('.main-img-layer').css('opacity', '1');
    } else {
        $(this).addClass('bg-hide btn-danger');
        $('.main-img-layer').css('opacity', '0');
        $(this).find('span').html('Hidden');
    }
})

// ============== Image & Wrapper Default Position =============

$('.layer-details-panel .btn-pos').on('click', function(){
    var activeAttribute = $(this).closest('.layer-details-panel').attr('data-layer-doc');
    var activeLayer = $('.scale-doc-section').find(`[data-layer-doc="${activeAttribute}"]`);
    activeLayer.css('top', '0px');
    activeLayer.css('left', '0px');
})
$('.layer-details-panel .btn-img-pos').on('click', function(){
    var activeAttribute = $(this).closest('.layer-details-panel').attr('data-layer-doc');
    var activeLayer = $('.scale-doc-section').find(`[data-layer-doc="${activeAttribute}"]`);
    var activeLayerImage = activeLayer.find('img');
    activeLayerImage.css('top', '0px');
    activeLayerImage.css('left', '0px');
})

// ============== Color Picker =============


$('#demo1').colorpicker().on('change', function(){
    var inputAttr = $(this).attr('data-style');
    $(this).closest('li').find('.color').css("background-color", $(this).colorpicker('getValue', '#ffffff') );
    $( `.layer-settings input[data-style="${inputAttr}"]` ).trigger( 'input' );
});

$('#background-color').colorpicker().on('change', function(){
    $(this).closest('.layer-settings').find('.color').css("background-color", $(this).colorpicker('getValue', '#ffffff') );
    $('.scale-doc-section').css('background', $(this).colorpicker('getValue', '#ffffff'));
});

// ============== Active Layer Disabled =============

function activeLayerDisabled(){
    $('.layer-doc').removeClass('active-layer');

    // Enable Text Dragging
    $('.layer-text').draggable({
        disabled:false
    })
}

// ============== Change Picture ===============
$('.btn-change-pic input').on('change', function(){
    var activeAttrPicture = $(this).closest('label').attr('data-picture');
    switch(activeAttrPicture){
        case 'background':
        readURL(this, '.main-img-layer img');
        break;

        case 'layerpicture':
        var findLayerDoc = $(`.layer-doc[data-layer-doc="${$(this).closest('.layer-details-panel').attr('data-layer-doc')}"]`).find('img')[0];
        readURL(this, findLayerDoc);
        break;
    }
})
function readURL(input, filelocation) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(filelocation).attr('src', e.target.result);
        }
        $(input).next('.filename').html(input.files[0].name);
        var activeLayerAttr = $(input).closest('.layer-details-panel').attr('data-layer-doc');
        var activeDropdownLayer = $(input).closest('.layer-dropdown').find('.layer-dropdown-toggle').find(`[data-layer-doc="${activeLayerAttr}"]`);
        activeDropdownLayer.attr('data-picture-name', input.files[0].name);
        reader.readAsDataURL(input.files[0]);
        setTimeout(function(){
            $(input).closest('.layer-details-panel').find('.layer-img').find('img').attr('src', $(filelocation).attr('src'));
            activeDropdownLayer.find('.layer-right').find('img').attr('src', $(filelocation).attr('src'));
        }, 1000);
        $(input).val(null);
    }
}
// ============== Saving File =============
$('.save-btn').on('click', function(){
    $('.loader-na').css('display', 'block');
    $('.layer-doc').removeClass('active-layer');
    $('.new-document').addClass('savingFinalImage');
    $('.scale-doc-section').css('transform', 'none');
    var currentScaleDoc = $('.doc-scale').attr('data-current-scale');
    setTimeout(function(){htmlToCanvasImg(currentScaleDoc)}, 3000);
    
})
function htmlToCanvasImg(currentScaleDoc){
    html2canvas(document.querySelector(".scale-doc-section"), {
        scale: 1,
        allowTaint: true,
        backgroundColor: null
    }).then(canvas => {
        $('.final-img').html(canvas);
        $('.new-document').removeClass('savingFinalImage');
        $('.scale-doc-section').css('transform', `scale(${parseFloat(currentScaleDoc).toFixed(1)})`);
        setTimeout(function(){$('.loader-na').css('display', 'none');}, 1000);
    });
}

// ========== Sidebar Collapsed Panel
$('.sidebar-collapsed-icons li').click(function(){
    $('.sidebar-collapsed-icons li').css('pointer-events', 'none');
    var condition = $(this).hasClass('active');
    $('.sidebar').addClass('sidebar-active');
    $('.main-layout').addClass('sb-active');
    $('.card').css('display', 'none');
    $($(this).attr('data-target')).closest('.card').css('display', 'block');
    $('.sidebar-collapsed-icons li').removeClass('active');
    setTimeout(function(){ $('.sidebar-collapsed-icons li').css('pointer-events', 'auto') }, 1000);
    if(condition) {
        $(this).removeClass('active')
    } else {
        $(this).addClass('active')
    }
});



function fitToScreen(){
    var documentHeight = $('.main-content').height();
    var templateHeight = $('.scale-doc-section').height();
    var documentWidth = $('.main-content').width();
    var templateWidth = $('.scale-doc-section').width();

    var heightDifference = documentHeight / templateHeight;
    var widthDifference = documentWidth / templateWidth;

    var scaleValue = function(){
        if((documentWidth / documentHeight) >= 1) {
            if((templateWidth / templateHeight) >= 1) {
                return heightDifference
            } else {
                return widthDifference
            }
        } else {
            if((templateWidth / templateHeight) >= 1) {
                return widthDifference
            } else {
                return heightDifference
            }
        }
    }
    $('.doc-scale').attr('data-current-scale', scaleValue());
    $('.scale-doc-section').css('transform-origin', `0 0 0`);
    $('.scale-doc-section').css('transform', `scale(${scaleValue()})`).promise().done(function(){
        setTimeout(function(){
            var afterScaleHeight = $('.scale-doc-section').get(0).getBoundingClientRect().height;
            var afterScaleWidth = $('.scale-doc-section').get(0).getBoundingClientRect().width;
            $('.new-document').css('width', `${afterScaleWidth}px`);
            $('.new-document').css('height', `${afterScaleHeight}px`);
        }, 600);
    })
}

function fitToOriginal(){
    var templateHeight = $('.scale-doc-section').height();
    var templateWidth = $('.scale-doc-section').width();
    $('.scale-doc-section').css('transform', `scale(1)`);
    $('.new-document').css('width', `${templateWidth}px`);
    $('.new-document').css('height', `${templateHeight}px`);
    $('.doc-scale').attr('data-current-scale', 1);
}

$(document).ready(function(){
    fitToScreen();
})


$('.layer-set h3').click(function(){
    $('.layer-set').removeClass('active-layer-effect');
    $(this).closest('.layer-settings').addClass('effect-active');
    $(this).closest('.layer-set').addClass('active-layer-effect');
});

$('.btn-back-setting').click(function(){
    $('.layer-set').removeClass('active-layer-effect');
    $('.layer-settings').removeClass('effect-active');
})


// Layer Panel Open on selecting layer
$('.scale-doc-section').delegate('.layer-doc', 'click', function(){
    var attr = $(this).attr('data-layer-doc');
    var activeLayerGroup = $('.card').find(`[data-layer-doc="${attr}"]`).closest('.collapse').get(0).id;
    var layerEffectToggleBtn = $(`.sidebar-collapsed-icons li[data-target='#${activeLayerGroup}']`);
    if(layerEffectToggleBtn.hasClass('active')) {
        $(`.layer-dropdown li[data-layer-doc='${attr}']`).trigger('click');
    } else {
        layerEffectToggleBtn.trigger('click');
        $(`.layer-dropdown li[data-layer-doc='${attr}']`).trigger('click');        
    }
});

$('.scale-doc-section').delegate('.layer-text', 'click', function(){
    if(window.getSelection().type == 'Caret') {
        selectText($(this).find('.text-content').get(0));
    }
})
// Select Text on click
function selectText(containerid) {
    var range = document.createRange();
    range.selectNodeContents(containerid);
    var selection = window.getSelection();
    selection.removeAllRanges();
    selection.addRange(range);
}


$('.sidebar-toggle').click(function() {
    $(this).toggleClass('active');
    $('.main-layout').toggleClass('sb-active');
    $('.sidebar').toggleClass('sidebar-active');
})