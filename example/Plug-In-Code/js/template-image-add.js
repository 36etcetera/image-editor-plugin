// Admin Js

// layer settings admin
$('#layer-setting-admin a').on('click', function(){
    var currentValue = $(this).attr('data-value');
    var settingPanel = $('.layer-settings');
    var buttonHtm = $('.layer-dropdown-admin .dropdown-toggle');
    buttonHtm.html(currentValue);
    settingPanel.removeClass('disabled');
    settingPanel.attr('data-layer', currentValue);

    if($(this).attr('data-bradius') == 'circle') {
        $('.btn-circle-layer').addClass('btn-circle-active btn-success');
        settingPanel.find('[data-effect="bradius"]').val('0');
        settingPanel.find('[data-effect="bradius"]').attr('disabled', 'disabled');
    } else {
        layerSwitchDetails(settingPanel, '0', 'bradius', this);
        $('.btn-circle-layer').removeClass('btn-circle-active btn-success');
        settingPanel.find('[data-effect="bradius"]').removeAttr('disabled');
    }
    

    layerSwitchDetails(settingPanel, '0', 'width', this);
    layerSwitchDetails(settingPanel, '0', 'height', this);
    layerSwitchDetails(settingPanel, '0', 'positionX', this);
    layerSwitchDetails(settingPanel, '0', 'positionY', this);
});

$('.layer-settings input').on('input', function(){
    var activeVal = $(this).val();
    var activeAttr = $(this).attr('data-effect');
    var activeLayer = $('.layer-settings').attr('data-layer');
    var activeLayerCtrl = $('#layer-setting-admin').find(`[data-value=${activeLayer}]`);
    var docLayer = $('.preview-shape').find(`[data-layer=${activeLayer}]`);
    var valueSpan = $(this).closest('.form-group').find('.valueShow');
    activeLayerCtrl.attr(`data-${activeAttr}`, activeVal);
    valueSpan.html(`${activeVal}px`);

    switch(activeAttr){
        case 'width':
        docLayer.css('width', `${activeVal/2.6}px`);
        break;
        case 'height':
        docLayer.css('height', `${activeVal/2.6}px`);
        break;
        case 'bradius':
        docLayer.css('border-radius', `${activeVal/2.6}px`);
        break;
        case 'positionX':
        docLayer.css('left', `${activeVal/2.6}px`);
        break;
        case 'positionY':
        docLayer.css('top', `${activeVal/2.6}px`);
        break;
    } 
})

$('.btn-circle-layer').on('click', function(){
    var btnCircle = $(this);
    var btnPrevInput = btnCircle.closest('.form-group').find('input');
    var btnPrevValueSpan = btnCircle.closest('.form-group').find('.valueShow');
    var activeLayerAttr = $(this).closest('.layer-settings').attr('data-layer');
    var activeLayerDoc = $('.preview-shape').find(`[data-layer="${activeLayerAttr}"]`);
    var activeLayerCtrl = $('#layer-setting-admin').find(`[data-value=${activeLayerAttr}]`);
    btnPrevInput.val(0);
    
    if(btnCircle.hasClass('btn-circle-active')) {
        btnCircle.removeClass('btn-circle-active btn-success');
        btnPrevInput.removeAttr('disabled');
        btnPrevValueSpan.html('0px');
        activeLayerDoc.css('border-radius', '0px');
        activeLayerCtrl.attr('data-bradius', '0');
    } else {
        btnCircle.addClass('btn-circle-active btn-success');
        btnPrevInput.attr('disabled', 'disabled');
        btnPrevValueSpan.html('100%');
        activeLayerDoc.css('border-radius', '100%');
        activeLayerCtrl.attr('data-bradius', 'circle');
    }
})

function layerSwitchDetails(layerPanel, defaultValue, dataEffect, $this){
    var activeDataInput = layerPanel.find(`[data-effect="${dataEffect}"]`);
    var activeEffectPresent = $($this).attr(`data-${dataEffect}`);
    var valueSpan = activeDataInput.closest('.form-group').find('.valueShow');

    if(typeof activeEffectPresent !== 'undefined') {
        activeDataInput.val(activeEffectPresent);
        valueSpan.html(`${activeEffectPresent}px`);
    } else {
        activeDataInput.val(defaultValue);
        valueSpan.html(`${defaultValue}px`);
    }
}
$('.btn-temp-preview').click(function(){
    htmlToCanvasImg();
})
function htmlToCanvasImg(){
    var previeWidth = $('.preview-shape').width();
    var previeScale = 2.5;
    html2canvas(document.querySelector(".preview-shape"), {
        scale: previeScale,
        allowTaint: true,
        backgroundColor: null
    }).then(canvas => {
        $('.final-temp-img').html(canvas);
    });
}