// Admin Js

// Filter Add

$('.filter-color').colorpicker().on('change', function(){
    $(this).closest('.input-color-grp').find('.input-group-addon').css("background-color", $(this).colorpicker('getValue', '#ffffff') );
    $('.filter-div').css('background', $(this).colorpicker('getValue', '#ffffff'));
    $(this).attr('data-color-pick', $(this).val());
});

$('.btn-preview').on('click', () => {
    $('.filter-div').width(300);
    $('.filter-div').height(200);
    setTimeout(saveImge, 2000);
})
function saveImge(){
    html2canvas(document.querySelector(".filter-div"), {
        scale: 10,
        allowTaint: true,
        backgroundColor: null
    }).then(canvas => {
        $('.preview-filter-png').html(canvas);
        $('.filter-div').width('100%');
        $('.filter-div').height('100%');
    });
}

$('#gradientCheck').on('click', function(){
    var firstColor = $('.gradientbox').find('[data-gradient="first"]');
    var secondColor = $('.gradientbox').find('[data-gradient="second"]');
    if($(this).is(':checked')) {
        $('.gradientbox').slideDown();
        activeGradient(firstColor.colorpicker('getValue', 'rgba(0,0,0,0)'), secondColor.colorpicker('getValue', 'rgba(0,0,0,0)'));
    } else {
        if($('.filter-color').attr('data-color-pick') !== 'transparent') {
            $('.filter-div').css('background', $('.filter-color').attr('data-color-pick'));
        } else {
            $('.filter-div').css('background', 'rgba(0,0,0,0)');
        }
        $('.gradientbox').slideUp();
    }
})


function activeGradient(color1, color2) {
    $('.filter-div').css('background', `linear-gradient(${color1}, ${color2})`);
    $('.gradient-strip-color').css('background', `linear-gradient(${color1}, ${color2})`);
}
var count = 0;
var countColorArr = [];
$('.btn-gradient-add').on('click', function(){
    count = count + 1;
    var layerToAdd = `<div class="form-group"><label for="fname">Color-${count}</label><div class="input-group input-color-grp"><input type="text" class="gradient-color form-control" placeholder="Select Color" data-gradient="Color-${count}" data-color-pick="transparent" aria-describedby="basic-addon1"><span class="input-group-addon"></span></div></div>`;
    
    $('.gradient-color-sec').append(layerToAdd);

    $('.gradient-color').colorpicker().on('change', function(){
        $(this).closest('.input-color-grp').find('.input-group-addon').css("background-color", $(this).colorpicker('getValue', '#ffffff') );
        
        var countColorObj = {}; 
        var colorAll = "";
        countColorObj[`${$(this).attr('data-gradient')}`] = $(this).colorpicker('getValue', '#ffffff');

        if(_.isEmpty(_.find(countColorArr, $(this).attr('data-gradient')))) {
            countColorArr.push(countColorObj);   
        } else {
            var activeIndex = _.indexOf(countColorArr, _.find(countColorArr, $(this).attr('data-gradient')));
            countColorArr[activeIndex][$(this).attr('data-gradient')] = $(this).colorpicker('getValue', '#ffffff');
        }
        
        for(var i=0; i <= countColorArr.length - 1; i++) {
            colorAll = colorAll + ', ' + countColorArr[i][`Color-${i + 1}`]
        }
        $('.gradient-strip-color').css('background', `linear-gradient(to right${colorAll})`);
        $('.filter-div').css('background', `linear-gradient(to right${colorAll})`);
        
    });
})
                                
