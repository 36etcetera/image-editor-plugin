// Admin Js

// Template Add
var widthInputFind = $('.form-content').find('[data-range="width"]');
var heightInputFind = $('.form-content').find('[data-range="height"]');
var scaleInputFind = $('.form-content').find('[data-range="scale"]');
var radiusInputFind = $('.form-content').find('[data-range="radius"]');

radiusMaxValue();

$('.form-content input[type="range"]').on('input', function(){
    var switchAttr = $(this).attr('data-range');
    var previewShape = $('.preview-shape');
    var valueSpan = $(this).closest('.form-group').find('.valueShow');
    var prevValue = $(this).val();
    var afterScaleWidth, afterScaleHeight; 
    var scaleWidth = $('#scaleWidth');
    var scaleHeight = $('#scaleHeight');
    previewShape.css('display', 'block');
    
    switch(switchAttr){
        case 'width':
        previewShape.css('width', `${prevValue/2.6}px`);
        valueSpan.html(`${prevValue}px`);
        scaleWidth.val(`${parseFloat(scaleInputFind.val() * prevValue).toFixed(1)}px`);
        radiusMaxValue();
        break;

        case 'height':
        previewShape.css('height', `${prevValue/2.6}px`);
        valueSpan.html(`${prevValue}px`);
        scaleHeight.val(`${parseFloat(scaleInputFind.val() * prevValue).toFixed(1)}px`);
        break;

        case 'scale':
        valueSpan.html(`x${prevValue}`);
        scaleWidth.val(`${parseFloat(prevValue * widthInputFind.val()).toFixed(1)}px`);
        scaleHeight.val(`${parseFloat(prevValue * heightInputFind.val()).toFixed(1)}px`);
        break;

        case 'radius':
        previewShape.css('border-radius', `${prevValue/2.6}px`);
        valueSpan.html(`${prevValue}px`);
        break;
    }
});

$('.btn-circle-temp').click(function(){
    var previewShape = $('.preview-shape');
    var valueSpan = $(this).closest('.form-group').find('.valueShow');

    if($(this).hasClass('circle-active')) {
        $(this).removeClass('circle-active');
        radiusInputFind.removeAttr('disabled');
        previewShape.css('border-radius', `${radiusInputFind.val()}px`);   
        $(this).html('Circle or Oval');
        valueSpan.html(`${radiusInputFind.val()}px`);
    } else {
        radiusInputFind.attr('disabled', 'disabled');
        previewShape.css('border-radius', '100%');
        $(this).html('Remove Circle or Oval');
        $(this).addClass('circle-active');
        valueSpan.html('100%');
    }
})

function radiusMaxValue(){
    radiusInputFind.attr('max', function(){
        if(widthInputFind.val() < 1 || widthInputFind.val() == 0) {
            return 0
        } else {
            return widthInputFind.val() / 2;
        }
    })
}

