# encoding: UTF-8
$:.push File.expand_path('../lib', __FILE__)
require 'solidus_on_demand_products/version'

Gem::Specification.new do |s|
  s.name        = 'solidus_on_demand_products'
  s.version     = SolidusOnDemandProducts::VERSION
  s.summary     = 'On demand Product'
  s.description = 'This extension give user a feature to create their own product and order it.'
  s.license     = 'BSD-3-Clause'

  s.author    = 'Thirty Six Etecetera'
  s.email     = 'dev-support@36etcetera.com'
  s.homepage  = 'http://www.36etcetera.com'

  s.files = Dir["{app,config,db,lib}/**/*", 'LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'solidus_core', '~> 2.6.0'

  s.add_development_dependency 'capybara'
  s.add_development_dependency 'poltergeist'
  s.add_development_dependency 'coffee-rails'
  s.add_development_dependency 'sass-rails'
  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_girl'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rubocop', '0.37.2'
  s.add_development_dependency 'rubocop-rspec', '1.4.0'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'sqlite3'
end
