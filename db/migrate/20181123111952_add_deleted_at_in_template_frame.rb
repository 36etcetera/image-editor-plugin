class AddDeletedAtInTemplateFrame < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_template_frames, :deleted_at, :datetime
  end
end
