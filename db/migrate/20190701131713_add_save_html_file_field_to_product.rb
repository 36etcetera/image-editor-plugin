class AddSaveHtmlFileFieldToProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_products, :save_html, :text
  end
end
