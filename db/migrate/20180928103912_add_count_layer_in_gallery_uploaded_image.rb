class AddCountLayerInGalleryUploadedImage < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_gallery_uploaded_images, :gallery_image_layer, :string
  end
end
