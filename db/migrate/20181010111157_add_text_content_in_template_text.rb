class AddTextContentInTemplateText < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_template_texts, :text_content, :string, default: "This is dummy text"
  end
end
