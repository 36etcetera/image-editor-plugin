class AddRotateToSpreeTemplateImages < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_template_images, :rotate, :decimal
  end
end
