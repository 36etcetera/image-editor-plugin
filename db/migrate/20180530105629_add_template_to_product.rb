class AddTemplateToProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_products, :template_id, :integer
  end
end
