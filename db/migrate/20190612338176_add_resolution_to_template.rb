class AddResolutionToTemplate < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_templates, :resolution, :integer
  end
end
