class RemoveScaleFromTemplate < ActiveRecord::Migration[5.1]
  def change
    remove_column :spree_templates, :scale
  end
end
