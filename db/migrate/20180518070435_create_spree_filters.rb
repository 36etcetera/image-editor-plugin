class CreateSpreeFilters < ActiveRecord::Migration[5.1]
  def change
    create_table :spree_filters do |t|
      t.string :name
      t.datetime :deleted_at
      t.string :slug
      
      t.timestamps
    end
  end
end
