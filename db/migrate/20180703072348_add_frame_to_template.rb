class AddFrameToTemplate < ActiveRecord::Migration[5.1]
  def change
    add_attachment :spree_templates, :frame
  end
end
