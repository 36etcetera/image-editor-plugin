class RemoveFrameFieldFromTemplate < ActiveRecord::Migration[5.1]
  def change
    remove_attachment :spree_templates, :frame
  end
end
