class RemovePriceWeightFromTemplate < ActiveRecord::Migration[5.1]
  def change
    remove_column :spree_templates, :price
    remove_column :spree_templates, :weight
  end
end
