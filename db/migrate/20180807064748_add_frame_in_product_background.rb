class AddFrameInProductBackground < ActiveRecord::Migration[5.1]
  def change
    add_attachment :spree_product_backgrounds, :product_frame
  end
end
