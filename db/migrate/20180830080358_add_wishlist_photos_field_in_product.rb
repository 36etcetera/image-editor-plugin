class AddWishlistPhotosFieldInProduct < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_products, :wishlist_photos, :boolean
  end
end
