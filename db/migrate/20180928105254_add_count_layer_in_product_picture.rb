class AddCountLayerInProductPicture < ActiveRecord::Migration[5.1]
  def change
    add_column :spree_product_pictures, :product_picture_layer, :string
  end
end
